import http from './http-common';
import { AcademicYearListItem, AccessRight, MonthsReportSchedule, Schedule, VersionInfo } from '../interfaces/calendar-plan-interfaces';
import { calendarPlanStore } from './calendar-plan-store';

export type TBlobHeaders = {
  'content-disposition' : string
}

export type TResponse
    < TDataType extends string[] | Blob | unknown, THeaders> = {
  data: TDataType,
  headers: THeaders;
  status: number,
  statusText: string
}

export class CalendarPlanDataService {
  getVersion () : Promise<TResponse<VersionInfo, unknown>> {
    return http.get('/version');
  }

  getAccessRight () : Promise<TResponse<AccessRight, unknown>> {
    return http.get('/auth/get-access-right');
  }

  getAcademicYears () : Promise<TResponse<AcademicYearListItem[], unknown>> {
    return http.get('/academic-years');
  }

  buildCalendarPlanFilename (academicYear : string, name : string, ext: string) : string {
    return `${(academicYear) || 'base'}-${name}.${ext}`;
  }

  monthReport (month: number, schedule : MonthsReportSchedule) : Promise<TResponse<Blob, TBlobHeaders>> {
    return http.post(`/calendar-plan/month-report/${month}`, schedule, { responseType: 'blob' });
  }

  allMonthsReport (schedule : MonthsReportSchedule) : Promise<TResponse<Blob, TBlobHeaders>> {
    return http.post('/calendar-plan/month-report', schedule, { responseType: 'blob' });
  }

  exportCalendarPlan (schedule : Schedule) : Promise<TResponse<Blob, TBlobHeaders>> {
    return http.post('/calendar-plan/export', schedule, { responseType: 'blob' });
  }

  exportCalendarPlanAsExcel (schedule : Schedule) : Promise<TResponse<Blob, TBlobHeaders>> {
    return http.post('/calendar-plan/export-excel-templates', schedule, { responseType: 'blob' });
  }

  transformExcelFilesToWord (schedule : Schedule, filesToUpload : Map<string, ArrayBuffer>) : Promise<TResponse<Blob, TBlobHeaders>> {
    const formData = new FormData();
    formData.append('calendarPlan', calendarPlanStore.scheduleTemplate);

    filesToUpload.forEach((bytes: ArrayBuffer, filename: string) => {
      if (filename.includes('.xlsx')) {
        const blob = new Blob([new Uint8Array(bytes)], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        formData.append(filename, blob);
      }
    });

    return http.post('/calendar-plan/transform-excel-files-to-word', formData,
      {
        headers:
        {
          'Content-Type': 'multipart/form-data'
        },
        responseType: 'blob'
      });
  }
}

export const calendarPlanDataService = new CalendarPlanDataService();
