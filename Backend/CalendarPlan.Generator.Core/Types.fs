﻿namespace CalendarPlan.Generator.Core.Types

open System

type RangeType = Year=0 | Holiday=1 | Transition=2 | Vacation=3
type SubjectProgramType = General=0 | Retention=1 | First=2 | Second=3

type Range = { From: DateTime; To: DateTime }

type ReferenceListItem<'a> = { Id: 'a; Name: string; IsActive: bool }

type LessonTitle = {Title: string; Semester: int; IsAnchor: bool; Weight: float}
type LessonTitleSubject = { Id: int; Name: string; Class: int; ProgramType: SubjectProgramType; LessonTitles: LessonTitle array }

type AcademicYear =
    { Id: int
      AcademicYear: string
      Range: Range
      Holidays: Range seq
      Transitions: Range seq
      Vacations: Range seq
      NonWorkingDays: DayOfWeek seq
      IsActive: bool }

type ScheduleItem =
    { LessonNumber: int
      Subject: string
      Day: DayOfWeek
      Offset: int
      Period: float
      Load: float option }

type Schedule =
    { AcademicYearId: int
      StudentClass: int option
      Schedules: ScheduleItem seq }

type Meta = { Teacher: string; Student: string }

type MonthsReportSchedule = { Meta: Meta; Schedule: Schedule }

type EventItem =
    { Index: int
      Date: DateTime
      Load: float
      LessonNumber: int }

type SubjectEvents =
    { Title: string
      Load: float
      Events: EventItem seq }

type ExportExcelItem =
    { Subject: string
      Semester: int
      Indexes: int seq }

type ImportExcelItem =
    { SectionIndex: int
      SectionTitle: string
      LessonIndex: int
      LessonTitle: string }

type Lesson =
    { Index: int
      Title: string
      Load: float
      Date: DateTime }

type Section =
    { Index: int
      Title: string
      Lessons: Lesson array }

type Subject =
    { Title: string
      Load: float
      IsSectionVisible: bool
      Sections: Section array }

type CalendarPlanDocument = { Subjects: Subject array }
