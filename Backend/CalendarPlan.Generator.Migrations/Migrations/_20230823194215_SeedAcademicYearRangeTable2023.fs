namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20230823194215L, "Seed AcademicYearRange Table 2023")>]
type _20230823194215_SeedAcademicYearRangeTable2023() =
    inherit Migration()

    let upScript = @"
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (30, '2023-2024', 0, '2023-09-01', '2024-05-31');

insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (31, '2023-2024', 3, '2023-10-23', '2023-10-29');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (32, '2023-2024', 3, '2023-12-23', '2024-01-07');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (33, '2023-2024', 3, '2024-03-25', '2024-03-31');

insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo, Class) values (34, '2023-2024', 3, '2024-02-19', '2024-02-25', 1);"

    override this.Up() =
        base.Execute(upScript)
    override this.Down() =
        base.Execute(@"delete from AcademicYearRange where Id > 29")
