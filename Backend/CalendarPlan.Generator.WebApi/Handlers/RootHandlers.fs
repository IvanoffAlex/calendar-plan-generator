module CalendarPlan.Generator.WebApi.Handlers.RootHandlers

open System.Threading.Tasks
open CalendarPlan.Generator
open CalendarPlan.Generator.WebApi
open CalendarPlan.Generator.WebApi.ViewModels
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Logging
open Microsoft.OpenApi.Models
open Oxpecker
open Oxpecker.OpenApi.Configuration

type WebApiMarker = interface end


type RootMeta() =
    let metaSummary = "Root"
    let metaTags = [| OpenApiTag(Name = "Root endpoints") |]

    member this.Pong =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Simple API answer \"pong\"" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<string>, [| "text/plain" |]) ])

    member this.Version =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Return system information" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<VersionInfo>) ])

let Meta = RootMeta()

let RedirectToUI (ctx: HttpContext) =
    let configuration = ctx.GetService<IConfiguration>()
    let webClientUrl = configuration.GetValue("WebClientUrl")
    redirectTo webClientUrl false ctx

let Pong ctx =
    text "pong" ctx

let Version (ctx: HttpContext) =
    task {
            let marker = typeof<WebApiMarker>.Assembly
            let info = SystemInfo().Info marker
            let serializer = ctx.GetJsonSerializer()
            return! serializer.Serialize(info, ctx, false)
    }
    :> Task

let ErrorHandler (ctx: HttpContext) (next: RequestDelegate) =
    task {
        try
            return! next.Invoke(ctx)
        with
        | :? ModelBindException
        | :? RouteParseException as ex ->
            let logger = ctx.GetLogger()
            logger.LogWarning(ex, "Unhandled 400 error")
            ctx.SetStatusCode StatusCodes.Status400BadRequest
            return! json { StatusCode = StatusCodes.Status400BadRequest; Message = (string ex) } ctx
        | ex ->
            let logger = ctx.GetLogger()
            logger.LogError(ex, "Unhandled 500 error")
            ctx.SetStatusCode StatusCodes.Status500InternalServerError
            return! json { StatusCode = StatusCodes.Status500InternalServerError; Message = (string ex) } ctx
    }
    :> Task

let NotFoundHandler (ctx: HttpContext) =
    let logger = ctx.GetLogger()
    logger.LogWarning("Unhandled 404 error")
    ctx.SetStatusCode StatusCodes.Status404NotFound
    json { StatusCode = StatusCodes.Status404NotFound; Message = "Page not found!" } ctx
