Push-Location
Set-Location "./Frontend/CalendarPlan.Generator.UI"
npm ci
npm run-s build
Pop-Location

Push-Location
Set-Location "./Backend/CalendarPlan.Generator.WebApi"
dotnet build
If ((Test-Path -Path "./bin/Debug/net8.0/ui") -eq $true)
  { Remove-Item "./bin/Debug/net8.0/ui" -Recurse -Force }
Copy-Item -Path "./../../Frontend/CalendarPlan.Generator.UI/dist" -Destination "./bin/Debug/net8.0/ui" -Recurse 
Invoke-Expression 'cmd /c start pwsh -Command { dotnet run }'
Pop-Location
