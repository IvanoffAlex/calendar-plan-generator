/* eslint-disable no-unused-vars */
import { Guid } from 'guid-typescript/dist/guid';

export enum DayOfWeek {
    Sunday = 0,
    Monday = 1,
    Tuesday = 2,
    Wednesday = 3,
    Thursday = 4,
    Friday = 5,
    Saturday = 6
}

export interface ScheduleItem {
    Id?: Guid
    LessonNumber: number
    Subject: string
    Day: DayOfWeek
    Offset: number
    Period: number
    Load?: number
}

export interface Schedule {
    AcademicYearId: number
    StudentClass?: number
    Schedules: ScheduleItem[]
}

export interface Event {
    Index: number
    Date: Date
    LessonNumber: number
}

export interface MonthReportOptions {
    Teacher: string
    Student: string
    Class: number
}

export interface MonthsReportSchedule {
    Meta: MonthReportOptions
    Schedule: Schedule
}

export interface ScheduleItemRendererProps {
    item: ScheduleItem
}

export interface AssemblyInfo {
    AssemblyName: string
    Version: string
}

export interface VersionInfo {
    Assemblies: AssemblyInfo[]
    Runtime: string
}

export interface AccessRight {
    Name: string
    CanManage: boolean
    IsAuthenticated: boolean
}

export interface AcademicYearListItem {
    Id: number,
    Name: string,
    IsActive: boolean
}
