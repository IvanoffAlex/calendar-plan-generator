module CalendarPlan.Generator.WebApi.Handlers.Routes

open CalendarPlan.Generator.WebApi.Handlers.Authorization
open Oxpecker
open Oxpecker.OpenApi
open Microsoft.AspNetCore.Builder
open type Microsoft.AspNetCore.Http.TypedResults

let private supervisorAuthorization() =
    configureEndpoint _.RequireAuthorization(Roles.Supervisor)

let Endpoints = [
    GET [ route "/" <| RootHandlers.RedirectToUI ]
    subRoute "/auth" [
        GET [
            route "/auth-google" <| AuthenticationHandlers.SigninGoogle
            route "/logoff" <| AuthenticationHandlers.SignOut
        ]
    ]
    subRoute "api/v2" [
        GET [
            route "/ping" <| RootHandlers.Pong |> addOpenApi(RootHandlers.Meta.Pong)
            route "/version" <| RootHandlers.Version |> addOpenApi(RootHandlers.Meta.Version)
        ]
        GET [ route "/auth/get-access-right" <| AuthenticationHandlers.GetAccessRights |> addOpenApi(AuthenticationHandlers.Meta.GetAccessRights) ]
        AcademicYearsHandlers.SetupAcademicYearsEndpoints()
        subRoute "/calendar-plan" [
           POST [
                route "/export" <| CalendarPlanHandlers.ExportCalendarPlan |> addOpenApi(CalendarPlanHandlers.Meta.ExportCalendarPlan)
                route "/export-excel-templates" <| CalendarPlanHandlers.ExportExcelCalendarPlan |> addOpenApi(CalendarPlanHandlers.Meta.ExportExcelCalendarPlan)
                route "/transform-excel-files-to-word" <| CalendarPlanHandlers.TransformExcelFilesToWord |> addOpenApi(CalendarPlanHandlers.Meta.TransformExcelFilesToWord)
                route "/month-report" <| CalendarPlanHandlers.ExportAllMonthsHours |> addOpenApi(CalendarPlanHandlers.Meta.ExportAllMonthsHours)
                routef "/month-report/{%i}" <| CalendarPlanHandlers.ExportMonthHours |> addOpenApi(CalendarPlanHandlers.Meta.ExportMonthHours)
           ]
        ]
        subRoute "/subject-programs" [
            GET [
                routef "/{%i}/classes/{%i}/subjects" SubjectProgramHandlers.SearchSubject |> addOpenApi(SubjectProgramHandlers.Meta.SearchSubject)
            ]
        ]
        subRoute "/manage" [
            AcademicYearsHandlers.SetupManageAcademicYearsEndpoints()
        ] |> supervisorAuthorization()
    ]
]
