﻿module CalendarPlan.Generator.Writers.DocumentWriter
open System
open System.IO
open DocumentFormat.OpenXml
open DocumentFormat.OpenXml.Packaging
open DocumentFormat.OpenXml.Wordprocessing
open CalendarPlan.Generator.Core.Types
open System.Collections.Generic

let private justificationCenter = EnumValue<JustificationValues>(JustificationValues.Center)
let private justificationLeft = EnumValue<JustificationValues>(JustificationValues.Left)
let private workbookName = "теми"
let private cellRowOffset = 6

let private monthNames =
    dict [ (1, "січень"); (2, "лютий"); (3, "березень"); (4, "квітень"); (5, "травень"); (6, "червень");
           (7, "липень"); (8, "серпень"); (9, "вересень"); (10, "жовтень"); (11, "листопад"); (12, "грудень") ]

let inline toMap kvps =
    kvps
    |> Seq.map (|KeyValue|)
    |> Map.ofSeq

let private writeSectionTitle (section : Section) (table: Table) =
    let row, rowCells = WordWriter.CreateRow [
        { Text = $"Змістовна лінія \"{section.Title}\""; Width = 5000; Justification = justificationCenter }
        { Text = ""; Width = 0; Justification = justificationCenter }
        { Text = ""; Width = 0; Justification = justificationCenter }
        { Text = ""; Width = 0; Justification = justificationCenter }
        { Text = ""; Width = 0; Justification = justificationCenter } ]
    WordWriter.MergeCells rowCells
    table.Append row

let private writeLessons (section : Section) (table: Table) =
    let lessonRows =
        section.Lessons
        |> Seq.map (fun item ->
           let row,_ = WordWriter.CreateRow [
               { Text = item.Index.ToString(); Width = 400; Justification = justificationCenter }
               { Text = item.Title; Width = 2000; Justification = justificationLeft }
               { Text = $"%g{item.Load}"; Width = 600; Justification = justificationCenter }
               { Text = item.Date.ToString("dd-MM"); Width = 700; Justification = justificationCenter }
               { Text = ""; Width = 600; Justification = justificationCenter } ]
           row)
    table.Append lessonRows

let private writeSubjectTable isFirst subject (document: Document) =
    if not <| isFirst then WordWriter.WriteBreakPage document
    printfn $"Write subject \"%s{subject.Title}\" with load %g{subject.Load} per week)"

    let text = $"%s{subject.Title} (%g{subject.Load} години на тиждень)"
    WordWriter.WriteParagraphText document text (EnumValue<JustificationValues>(JustificationValues.Center))

    let subjectTable =
        WordWriter.CreateTable [
            { Text = "№ уроку"; Width = 400; Justification = justificationCenter }
            { Text = "Тема"; Width = 2000; Justification = justificationCenter }
            { Text = "Кількість годин"; Width =  600; Justification = justificationCenter}
            { Text = "Дата проведення"; Width =  700; Justification = justificationCenter }
            { Text = "Примітки"; Width = 600; Justification = justificationCenter }
        ]

    subject.Sections |> Array.iter (fun section -> (
        if subject.IsSectionVisible then writeSectionTitle section subjectTable
        writeLessons section subjectTable))

    WordWriter.AppendToDocument document subjectTable

let private writeSubjectTables (wordDocument : WordprocessingDocument) subjects =
    subjects
    |> Array.iteri (fun index subject -> writeSubjectTable (index = 0) subject wordDocument.MainDocumentPart.Document)

    wordDocument.MainDocumentPart.Document.Save()

let private prepareCellsData excelItem =
    excelItem.Indexes
        |> Seq.mapi (fun index lessonIndex -> [
                ($"B%i{index+cellRowOffset}", excelItem.Semester)
                ($"G%i{index+cellRowOffset}", lessonIndex)
            ])
        |> Seq.concat

let WriteCalendarPlanToStream calendarPlanDocument stream =
    use wordDocument = WordWriter.CreateWordDocumentInStream stream
    writeSubjectTables wordDocument calendarPlanDocument.Subjects

let WriteMonthReportToStream (subjects: IDictionary<DateTime,SubjectEvents seq>) academicYear studentClass meta stream =
    use wordDocument = WordWriter.CreateWordDocumentInStream stream

    let document = wordDocument.MainDocumentPart.Document

    let firstMonth = subjects.Keys |> Seq.head

    subjects |> toMap |> Map.iter (fun month events ->
        if month <> firstMonth then WordWriter.WriteBreakPage document

        let titleLines = seq [
            ($"Вчитель: %s{meta.Teacher}%s{String('\t', 5)}" +
             $"місяць %s{monthNames[month.Month]} " +
             $"%i{academicYear.Range.From.Year}-%i{academicYear.Range.To.Year} н. р.", justificationLeft)
            ($"Учень: %s{meta.Student}%s{String('\t', 6)}%i{studentClass} клас", justificationLeft)]
        WordWriter.WriteParagraphTextLines document titleLines

        let subjectTable =
            WordWriter.CreateTable [
                { Text = "Назва предметів"; Width = 1000; Justification = justificationCenter }
                { Text = "Кількість годин"; Width = 500; Justification = justificationCenter }
                { Text = "Дати проведення"; Width =  2200; Justification = justificationCenter} ]

        let eventRows =
            events |> Seq.map (fun event ->
                let row,_ = WordWriter.CreateRow [
                    { Text = event.Title; Width = 1000; Justification = justificationLeft }
                    { Text = $"%g{event.Events |> Seq.sumBy(_.Load)}"; Width = 500; Justification = justificationCenter }
                    { Text = event.Events |> Seq.map(fun x -> $"%i{x.Date.Day}") |> String.concat ", "
                      Width = 2200
                      Justification = justificationLeft } ]
                row)
        subjectTable.Append eventRows
        WordWriter.AppendToDocument document subjectTable

        let signLines = seq [
            ("", justificationLeft)
            ("Підпис та ПІБ вчителя __________________________________________________________________", justificationLeft)
            ("Підпис та ПІБ батьків  __________________________________________________________________", justificationLeft)
            ("Підпис заступника директора з НВР ______________________________________________________", justificationLeft) ]
        WordWriter.WriteParagraphTextLines document signLines)

    wordDocument.MainDocumentPart.Document.Save()

let WriteExcelItemToStream excelItem =
    let stream = new MemoryStream()
    let integerCellsData = prepareCellsData excelItem
    use excelDocument = ExcelWriter.OpenFromExcelTemplate "Templates" "topics-template.xlsx" stream
    let worksheetPart = ExcelWriter.GetWorksheetPart workbookName excelDocument
    ExcelWriter.WriteCellsData worksheetPart integerCellsData
    excelDocument.Save()
    excelDocument.Dispose()
    stream.Seek(0L, SeekOrigin.Begin) |> ignore
    stream

let ReadExcelData copyTo =
    use stream = new MemoryStream()
    copyTo stream
    let excelDocument = ExcelWriter.OpenFromStream(stream)
    let worksheetPart = ExcelWriter.GetWorksheetPart workbookName excelDocument
    let stringTable = excelDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>() |> Seq.head
    ExcelWriter.ReadCellsData stringTable.SharedStringTable worksheetPart cellRowOffset
