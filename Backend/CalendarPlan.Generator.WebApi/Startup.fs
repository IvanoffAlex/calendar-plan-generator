﻿module CalendarPlan.Generator.WebApi.Startup
open System
open CalendarPlan.Generator.Migrations
open CalendarPlan.Generator.WebApi.Handlers.Authorization
open CalendarPlan.Generator.WebApi.Handlers.Extensions
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.AspNetCore.Authentication.Google
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Oxpecker
open CalendarPlan.Generator.Core

let ExitCode = 0

let ConfigureApp (_: WebHostBuilderContext) (app : IApplicationBuilder) =
    app.UseCors("AllowAll")
        .UseResponseCaching()
        .UseStaticFiles()
        .ConfigureOxpecker()

let ConfigureLogging (builder : ILoggingBuilder) =
    let filter (l : LogLevel) = l.Equals LogLevel.Error
    builder.AddFilter(filter).AddConsole().AddDebug() |> ignore

let ConfigureServices (context : WebHostBuilderContext) (services : IServiceCollection) =

    services.AddResponseCaching()
        .ConfigureOxpeckerServices()
        .AddLogging(fun builder -> builder.AddFilter("Microsoft.AspNetCore", LogLevel.Warning) |> ignore)
        .AddSingleton<IAuthorizationHandler, RoleRequirementHandler>()
        .AddSingleton<Db.FSharpDapperFactory>()
        .AddSingleton<Serializers.IJsonSerializer>(SystemTextJson.Serializer(JsonSettings.DeserializationOptions)) |> ignore

    services
        .AddHttpContextAccessor()
        .AddAuthorization(fun options ->
            options.AddPolicy(Roles.Supervisor, fun p ->
                p.AddRequirements(RoleRequirement(Roles.Supervisor)) |> ignore))
        .AddAuthentication(fun options -> (
            options.DefaultScheme <- CookieAuthenticationDefaults.AuthenticationScheme
            options.DefaultChallengeScheme <- GoogleDefaults.AuthenticationScheme))
        .AddCookie()
        .AddGoogle(fun options ->
            options.ClientId <- context.Configuration.GetValue("Authentication:Google:ClientId")
            options.ClientSecret <- context.Configuration.GetValue("Authentication:Google:ClientSecret")
            options.CallbackPath <- context.Configuration.GetValue("Authentication:Google:CallbackUri")
            options.SaveTokens <- true) |> ignore

    let clientUri = Uri(context.Configuration.GetValue("WebClientUrl"))
    let origin = clientUri.GetLeftPart(UriPartial.Authority)
    services.AddCors(fun options ->
        options.AddPolicy("AllowAll", fun builder ->
             builder.AllowAnyHeader()
                    .WithOrigins(origin)
                    .WithMethods("GET", "POST", "PUT", "DELETE")
                    .AllowCredentials()
                    .WithExposedHeaders("Content-Disposition", "X-Suggested-Filename") |> ignore)) |> ignore

let WithMigration (services : IServiceProvider) =
    let configuration = services.GetService<IConfiguration>()
    let useMigration = configuration.GetValue("Database:UseMigration")
    if useMigration
    then
        use scope = services.CreateScope()
        let services = scope.ServiceProvider
        let db = services.GetRequiredService<Db.FSharpDapperFactory>()
        Db.MigrateToLatest<MigrationMarker> db
