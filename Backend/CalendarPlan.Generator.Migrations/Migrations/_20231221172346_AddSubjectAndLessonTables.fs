namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20231221172346L, "Add Subject and LessonTitle tables")>]
type _20231221172346_AddSubjectAndLessonTables() =
    inherit Migration()

    let upScript = @"
create table Subject
(
    Id          INTEGER not null
        constraint Subject_pk
            primary key autoincrement,
    Name        TEXT    not null,
    Class       INTEGER not null,
    ProgramType INTEGER not null,
    CreatedOn   TEXT    not null,
    ModifiedOn  TEXT    not null
);

create table LessonTitle
(
    Id        INTEGER          not null
        constraint LessonTitle_pk
            primary key autoincrement,
    SubjectId INTEGER          not null
        constraint LessonTitle_Subject_Id_fk
            references Subject
            on update cascade on delete cascade,
    Title     TEXT             not null,
    Semester  INTEGER          not null,
    IsAnchor  INTEGER          not null,
    Weight    REAL default 1.0 not null
);
    "
    override this.Up() =
        base.Execute(upScript)
    override this.Down() =
        base.Execute(@"
drop table LessonTitle;
drop table Subject;
        ")
