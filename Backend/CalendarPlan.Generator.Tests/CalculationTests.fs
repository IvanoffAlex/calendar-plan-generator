namespace CalendarPlan.Generator.Tests
open System
open CalendarPlan.Generator.Core.Types
open CalendarPlan.Generator.Core
open Xunit

type CalculationTests() =
    let db = TestAssets.CreateDbFactory

    [<Fact>]
    member _.``Calculate subjects for the whole 2023-2024`` () =
        task {
            let expected = AssertionHelper.RetrieveExpectedTestAsset "Calculation.CalculateSubjects_2023-2024.json"
            let expectedSubjects = seq [("Subj_1",2.);("Subj_2",1.0);("Subj_3",0.75);("Subj_4",0.5);("Subj_5",0.5);("Subj_6",0.25);("Subj_7",0.25)]
            let schedule =
                {
                    AcademicYearId = 3
                    StudentClass = Some 1
                    Schedules = seq [
                        { LessonNumber = 1; Subject = "Subj_1"; Day = DayOfWeek.Tuesday; Offset = 1; Period = 1; Load = Some 1 }
                        { LessonNumber = 2; Subject = "Subj_4"; Day = DayOfWeek.Tuesday; Offset = 1; Period = 1; Load = Some 0.5 }
                        { LessonNumber = 3; Subject = "Subj_7"; Day = DayOfWeek.Tuesday; Offset = 1; Period = 1; Load = Some 0.25 }
                        { LessonNumber = 4; Subject = "Subj_3"; Day = DayOfWeek.Tuesday; Offset = 1; Period = 1; Load = Some 0.25 }
                        { LessonNumber = 1; Subject = "Subj_1"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 1 }
                        { LessonNumber = 2; Subject = "Subj_5"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 0.5 }
                        { LessonNumber = 3; Subject = "Subj_6"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 0.25 }
                        { LessonNumber = 4; Subject = "Subj_3"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 0.5 }
                        { LessonNumber = 5; Subject = "Subj_2"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 1 }
                    ]
                }
            let! academicYear = Db.GetAcademicYearAsync db schedule.AcademicYearId schedule.StudentClass

            let result = Calculation.CalculateSubjects schedule academicYear None

            AssertionHelper.AssertSubjectEvents result expected expectedSubjects
        }

    [<Fact>]
    member _.``Calculate subjects for October 2021`` () =
        task {
            let expected = AssertionHelper.RetrieveExpectedTestAsset "Calculation.CalculateSubjects_October2021.json"
            let expectedSubjects = seq [("Subj_1",1.);("Subj_4",1.0);("Subj_5",1.);("Subj_2",0.5);("Subj_3",0.25)]
            let schedule =
                {
                    AcademicYearId = 1
                    StudentClass = None
                    Schedules = seq [
                        { LessonNumber = 1; Subject = "Subj_1"; Day = DayOfWeek.Monday; Offset = 1; Period = 1; Load = Some 1 }
                        { LessonNumber = 1; Subject = "Subj_2"; Day = DayOfWeek.Thursday; Offset = 1; Period = 1; Load = Some 0.5 }
                        { LessonNumber = 1; Subject = "Subj_3"; Day = DayOfWeek.Wednesday; Offset = 1; Period = 1; Load = Some 0.25 }
                        { LessonNumber = 1; Subject = "Subj_4"; Day = DayOfWeek.Thursday; Offset = 1; Period = 1; Load = Some 1 }
                        { LessonNumber = 1; Subject = "Subj_5"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 1 }
                    ]
                }
            let! academicYear = Db.GetAcademicYearAsync db schedule.AcademicYearId schedule.StudentClass

            let result = Calculation.CalculateSubjects schedule academicYear (Some 10)

            AssertionHelper.AssertSubjectEvents result expected expectedSubjects
        }

    interface IClassFixture<DatabaseFixture>
