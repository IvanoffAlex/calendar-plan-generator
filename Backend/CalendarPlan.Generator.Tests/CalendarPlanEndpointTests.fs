namespace CalendarPlan.Generator.Tests
open System
open System.IO
open System.IO.Compression
open System.Net.Http
open System.Text
open System.Net
open CalendarPlan.Generator.Core.Types
open Xunit
open FsUnit.Xunit

type CalendarPlanEndpointTests() =
    let testServer = TestAssets.CreateTestServer
    let schedule = {
                        AcademicYearId = 2
                        StudentClass = Some 1
                        Schedules = seq [
                            { LessonNumber = 1; Subject = "Subj_1"; Day = DayOfWeek.Monday; Offset = 1; Period = 1; Load = Some 1 }
                            { LessonNumber = 2; Subject = "Subj_2"; Day = DayOfWeek.Monday; Offset = 1; Period = 1; Load = Some 1 }
                        ]
                    }

    [<Fact>]
    member _.``Export calendar plan as Word document`` () =
        task {
            use client = testServer.CreateClient()
            let json = AssertionHelper.ToJson schedule
            use content = new StringContent(json, Encoding.UTF8, "application/json")

            let! response = client.PostAsync("/api/v2/calendar-plan/export", content)

            response.StatusCode |> should equal HttpStatusCode.OK

            response.Headers |> should not' (be Empty)
            response.Headers.GetValues "Access-Control-Expose-Headers" |> Seq.exactlyOne |> should equal "Content-Disposition"
            response.Headers.GetValues "X-Content-Type-Options" |> Seq.exactlyOne |> should equal "nosniff"
            response.Headers.GetValues "Accept-Ranges" |> Seq.exactlyOne |> should equal "bytes"

            AssertionHelper.AssertHttpContentAsWordDocument response.Content "2022-2023"
        }

    [<Fact>]
    member _.``Export all months hours as Word document`` () =
        task {
            use client = testServer.CreateClient()
            let monthReportSchedule : MonthsReportSchedule =
                {
                    Meta = {
                        Student = "Student #1"
                        Teacher = "Teacher #1"
                    }
                    Schedule = schedule
                }
            let json = AssertionHelper.ToJson monthReportSchedule
            use content = new StringContent(json, Encoding.UTF8, "application/json")

            let! response = client.PostAsync("/api/v2/calendar-plan/month-report", content)

            response.StatusCode |> should equal HttpStatusCode.OK

            response.Headers |> should not' (be Empty)
            response.Headers.GetValues "Access-Control-Expose-Headers" |> Seq.exactlyOne |> should equal "Content-Disposition"
            response.Headers.GetValues "X-Content-Type-Options" |> Seq.exactlyOne |> should equal "nosniff"
            response.Headers.GetValues "Accept-Ranges" |> Seq.exactlyOne |> should equal "bytes"

            AssertionHelper.AssertHttpContentAsWordDocument response.Content "2022-2023"
        }

    [<Fact>]
    member _.``Export month hours as Word document`` () =
        task {
            use client = testServer.CreateClient()
            let month = 4
            let monthReportSchedule : MonthsReportSchedule =
                {
                    Meta = {
                        Student = "Student #1"
                        Teacher = "Teacher #1"
                    }
                    Schedule = schedule
                }
            let json = AssertionHelper.ToJson monthReportSchedule
            use content = new StringContent(json, Encoding.UTF8, "application/json")

            let! response = client.PostAsync($"/api/v2/calendar-plan/month-report/%i{month}", content)

            response.StatusCode |> should equal HttpStatusCode.OK

            response.Headers |> should not' (be Empty)
            response.Headers.GetValues "Access-Control-Expose-Headers" |> Seq.exactlyOne |> should equal "Content-Disposition"
            response.Headers.GetValues "X-Content-Type-Options" |> Seq.exactlyOne |> should equal "nosniff"
            response.Headers.GetValues "Accept-Ranges" |> Seq.exactlyOne |> should equal "bytes"

            AssertionHelper.AssertHttpContentAsWordDocument response.Content "2022-2023"
        }

    [<Fact>]
    member _.``Export calendar plan as Excel documents`` () =
        task {
            use client = testServer.CreateClient()
            let json = AssertionHelper.ToJson schedule
            use content = new StringContent(json, Encoding.UTF8, "application/json")

            let! response = client.PostAsync("/api/v2/calendar-plan/export-excel-templates", content)

            response.StatusCode |> should equal HttpStatusCode.OK

            response.Headers |> should not' (be Empty)
            response.Headers.GetValues "Access-Control-Expose-Headers" |> Seq.exactlyOne |> should equal "Content-Disposition"
            response.Headers.GetValues "X-Content-Type-Options" |> Seq.exactlyOne |> should equal "nosniff"

            AssertionHelper.AssertHttpContentAsZip response.Content "2022-2023" json
        }

    [<Fact>]
    member _.``Transform Excel files to Word Document`` () =
        task {
            let zipArchiveBytes = TestAssets.RetrieveEmbeddedResource "CalendarPlanEndpoint.InputExcelFiles.zip"
            use zipMemoryStream = new MemoryStream(zipArchiveBytes)
            use client = testServer.CreateClient()
            use zip = new ZipArchive(zipMemoryStream)
            let scheduleEntry = zip.Entries |> Seq.filter (fun x -> x.Name.EndsWith ".json") |> Seq.exactlyOne
            let zipSchedule = Encoding.UTF8.GetString (TestAssets.ToBytes scheduleEntry)
            let excelFiles = zip.Entries
                             |> Seq.filter (fun x -> x.Name.EndsWith ".xlsx")
                             |> Seq.map (fun x -> (x.Name, x.FullName, new MemoryStream(TestAssets.ToBytes x)))
            use multipartContent = new MultipartFormDataContent()
            excelFiles |> Seq.iter (fun (name, file, stream) -> (
                    let streamContent = new StreamContent(stream)
                    streamContent.Headers.ContentType <- Headers.MediaTypeHeaderValue("application/octet-stream");
                    multipartContent.Add(streamContent, name, file);
                ))
            multipartContent.Add(new StringContent(zipSchedule, Encoding.UTF8, "application/json"), "calendarPlan")

            let! response = client.PostAsync("/api/v2/calendar-plan/transform-excel-files-to-word", multipartContent)

            response.StatusCode |> should equal HttpStatusCode.OK

            zipSchedule |> should equal (AssertionHelper.ToJson schedule)

            response.Headers |> should not' (be Empty)
            response.Headers.GetValues "Access-Control-Expose-Headers" |> Seq.exactlyOne |> should equal "Content-Disposition"
            response.Headers.GetValues "X-Content-Type-Options" |> Seq.exactlyOne |> should equal "nosniff"
            response.Headers.GetValues "Accept-Ranges" |> Seq.exactlyOne |> should equal "bytes"

            AssertionHelper.AssertHttpContentAsWordDocument response.Content "2022-2023"
        }

    interface IClassFixture<DatabaseFixture>
