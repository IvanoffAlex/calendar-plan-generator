module CalendarPlan.Generator.WebApi.Handlers.Authorization

open CalendarPlan.Generator.WebApi
open CalendarPlan.Generator.WebApi.ViewModels
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Http
open Oxpecker

module Roles =
    let Supervisor = "Supervisor"

type RoleRequirement(role : string) =
    interface IAuthorizationRequirement
    member this.Role = role

type RoleRequirementHandler(httpContextAccessor : IHttpContextAccessor) =
    inherit AuthorizationHandler<RoleRequirement>()

    let returnUnauthorized (ctx: HttpContext) (context: AuthorizationHandlerContext) =
      ctx.SetStatusCode StatusCodes.Status401Unauthorized
      json { StatusCode = StatusCodes.Status401Unauthorized; Message = "Unauthorized. Required Supervisor role." } ctx |> ignore
      ctx.Response.CompleteAsync() |> ignore
      context.Fail()

    override this.HandleRequirementAsync(context, requirement) =
        task {
            match HttpContextUtility.IsSupervisors httpContextAccessor.HttpContext context.User with
            | true -> context.Succeed(requirement)
            | false -> returnUnauthorized httpContextAccessor.HttpContext context
        }
