module CalendarPlan.Generator.Writers.ExcelWriter
open System.IO
open DocumentFormat.OpenXml
open DocumentFormat.OpenXml.Packaging
open DocumentFormat.OpenXml.Spreadsheet

open CalendarPlan.Generator.Core.Types

type ExcelWriterMarker = interface end

let private writeIntegerToCell cells (ref: string, value: int) =
    let cell = cells |> Seq.find (fun (c: Cell) -> c.CellReference = StringValue ref)
    cell.CellValue <- CellValue(value)

let private getIntegerFromCell cells (ref: string) =
    let cell = cells |> Seq.tryFind (fun (c: Cell) -> c.CellReference = StringValue ref)
    match cell with
    | Some x -> (if isNull(x.CellValue) then None
                else
                    let hasValue,value = x.CellValue.TryGetInt()
                    if hasValue then Some value
                    else None)
    | None -> None

let private getStringFromCell (sharedStringTable: SharedStringTable) cells (ref: string) =
    let cell = cells |> Seq.tryFind (fun (c: Cell) -> c.CellReference = StringValue ref)
    match cell with
    | Some x -> (if isNull(x.CellValue) then None
                else
                    let hasValue, value = x.CellValue.TryGetInt()
                    if hasValue then Some (sharedStringTable |> Seq.item value).InnerText
                    else None)
    | None -> None

let private readRowData (sharedStringTable : SharedStringTable) cells cellRowOffset =
    let sectionIndex = getIntegerFromCell cells $"D%i{cellRowOffset}"
    match sectionIndex with
    | Some index -> Some {
            SectionIndex = index
            SectionTitle = getStringFromCell sharedStringTable cells $"E%i{cellRowOffset}" |> Option.get
            LessonIndex = getIntegerFromCell cells $"G%i{cellRowOffset}" |> Option.get
            LessonTitle = getStringFromCell sharedStringTable cells $"H%i{cellRowOffset}" |> Option.get
        }
    | None -> None

let OpenFromExcelTemplate resourceFolder resourceName (stream: Stream) =
    use templateStream = typeof<ExcelWriterMarker>.Assembly.GetManifestResourceStream
                             $"{typeof<ExcelWriterMarker>.Namespace}.{resourceFolder}.{resourceName}"
    templateStream.CopyTo stream
    SpreadsheetDocument.Open(stream, true)

let OpenFromStream (stream: Stream) =
    SpreadsheetDocument.Open(stream, false)

let GetWorksheetPart name (excelDocument : SpreadsheetDocument) =
    let worksheet =
        excelDocument.WorkbookPart.Workbook.Descendants<Sheet>()
        |> Seq.find (fun s -> s.Name = StringValue (name:string))
    downcast excelDocument.WorkbookPart.GetPartById worksheet.Id : WorksheetPart

let WriteCellsData (worksheetPart: WorksheetPart) integerCellsData =
    let cells = worksheetPart.Worksheet.Descendants<Cell>()
    integerCellsData |> Seq.iter (writeIntegerToCell cells)

let ReadCellsData (sharedStringTable : SharedStringTable) (worksheetPart: WorksheetPart) cellRowOffset =
    let cells = worksheetPart.Worksheet.Descendants<Cell>()
    Seq.initInfinite id
    |> Seq.map (fun index -> readRowData sharedStringTable cells (cellRowOffset+index))
    |> Seq.takeWhile (_.IsSome)
    |> Seq.map (fun item -> item |> Option.get)
