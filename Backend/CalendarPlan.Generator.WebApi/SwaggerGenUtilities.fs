module CalendarPlan.Generator.WebApi.SwaggerGenUtilities

open Microsoft.OpenApi.Models

let ConfigureMetaOperation summary tags (op : OpenApiOperation) =
    op.Summary <- summary
    op.Tags <- tags
    op

let ConfigureMetaOperationWithDescription description summary tags (op : OpenApiOperation) =
    op.Description <- description
    ConfigureMetaOperation summary tags op
