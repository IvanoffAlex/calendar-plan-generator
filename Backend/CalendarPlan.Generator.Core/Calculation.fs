﻿module CalendarPlan.Generator.Core.Calculation

open System
open System.Collections.Generic
open CalendarPlan.Generator.Core.Types

let private notExists ranges day =
    ranges |> Seq.exists (fun x -> x.From <= day && day <= x.To) |> not

let private isWorkingDate nonWorkingDays (day: DateTime) =
    nonWorkingDays |> Seq.contains day.DayOfWeek |> not

let private dateRange (startDate: DateTime) endDate =
    Seq.initInfinite float
    |> Seq.map startDate.AddDays
    |> Seq.takeWhile (fun data -> data <= endDate)

let private calculate item =
    (1. / item.Period) * (item.Load |> Option.defaultValue 1.)

let private applyTransition  academicYear date=
    let rule =
        academicYear.Transitions
        |> Seq.filter (fun range -> date = range.From)
        |> Seq.tryExactlyOne

    let newDateRange = rule |> Option.defaultValue { To = date; From = date }
    newDateRange.To

let private isApplicableMonth month (date: DateTime) =
    match month with
    | Some m -> date.Month = m
    | None -> true

let private offsetPeriodPredicate index scheduleItem =
    let period = scheduleItem.Period |> int
    let offset = scheduleItem.Offset
    (index + period - offset) % period = 0

let private calculateAcademicYearDates academicYear =
    dateRange academicYear.Range.From academicYear.Range.To
    |> Seq.filter (isWorkingDate academicYear.NonWorkingDays)
    |> Seq.filter (notExists academicYear.Holidays)
    |> Seq.filter (notExists academicYear.Vacations)

let private calculateSubjects (schedules: ScheduleItem seq) =
    schedules
    |> Seq.groupBy (_.Subject)
    |> Seq.map (fun (subject, items) -> (subject, items |> Seq.sumBy calculate))
    |> Seq.sortBy fst
    |> Seq.sortByDescending snd
    |> Seq.toArray

let private calculateScheduleItemDates (academicYear: AcademicYear) (academicYearDates : Lazy<seq<DateTime>>) month scheduleItem =
    academicYearDates.Value
    |> Seq.filter (fun (date: DateTime) -> date.DayOfWeek = scheduleItem.Day)
    |> Seq.map (applyTransition academicYear)
    |> Seq.filter (isApplicableMonth month)
    |> Seq.mapi (fun index date -> index, date)
    |> Seq.filter (fun (index, _) -> offsetPeriodPredicate index scheduleItem)
    |> Seq.map (fun (_, date) -> date, scheduleItem)

let private calculateScheduleItems plan academicYear academicYearDates month =
    plan.Schedules
    |> Seq.map (fun scheduleItem -> scheduleItem, calculateScheduleItemDates academicYear academicYearDates month scheduleItem)
    |> dict

let private calculateEvents (scheduleItems: IDictionary<ScheduleItem, seq<DateTime * ScheduleItem>>) subject =
    scheduleItems.Keys
    |> Seq.filter (fun scheduleItem -> scheduleItem.Subject = subject)
    |> Seq.map (fun scheduleItem -> scheduleItems[scheduleItem])
    |> Seq.concat
    |> Seq.sortBy (fun (date, scheduleItem) -> date, scheduleItem.LessonNumber)
    |> Seq.mapi (fun index (date, scheduleItem) ->
        { Index = index + 1
          Date = date
          Load = (scheduleItem.Load |> Option.defaultValue 1.)
          LessonNumber = scheduleItem.LessonNumber })
    |> Seq.toList

let private calculateSubjectItems schedule academicYear month =
    let academicYearDates = lazy (calculateAcademicYearDates academicYear)
    calculateSubjects schedule.Schedules
    |> Array.Parallel.map (fun (subject, load) ->
        let scheduleItems =
            calculateScheduleItems schedule academicYear academicYearDates month

        let events = calculateEvents scheduleItems subject

        { Title = subject
          Load = load
          Events = events })
    |> Array.toList

let CalculateSubjects plan academicYear month =
    printfn $"Start data transformation for %s{academicYear.AcademicYear} academic year"
    let subjectItems = calculateSubjectItems plan academicYear month
    printfn $"Calendar plan is calculated for %i{subjectItems.Length} subjects"
    subjectItems
