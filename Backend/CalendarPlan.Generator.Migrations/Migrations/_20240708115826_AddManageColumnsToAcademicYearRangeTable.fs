namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20240708115826L, "Introduce AcademicYear table")>]
type _20240708115826_AddManageColumnsToAcademicYearRangeTable() =
    inherit Migration()

    override this.Up() =
       base.Execute(@"
create table AcademicYear
(
    Id             INTEGER           not null
        constraint PK_academic_year_range
            primary key autoincrement,
    AcademicYear   TEXT              not null,
    NonWorkingDays TEXT              not null,
    IsActive       INTEGER default 1 not null
);

create unique index AcademicYear_AcademicYear_uindex
    on AcademicYear (AcademicYear);

INSERT INTO AcademicYear (Id, AcademicYear, NonWorkingDays, IsActive)
SELECT row_number() over (ORDER BY AcademicYear) Id, AcademicYear, '[6,0]', 1
FROM (SELECT DISTINCT AcademicYear FROM AcademicYearRange);

create table AcademicYearRange_tmp
(
    Id             INTEGER            not null
        constraint PK_academic_year_range
            primary key autoincrement,
    AcademicYearId INTEGER   not null
        constraint AcademicYearRange_AcademicYear_Id_fk
            references AcademicYear
            on update restrict on delete restrict,
    Type           INTEGER            not null,
    DateFrom       TEXT,
    DateTo         TEXT,
    Class          INTEGER default -1 not null

);

INSERT INTO AcademicYearRange_tmp (Id, AcademicYearId, Type, DateFrom, DateTo, Class)
SELECT Id,
       (SELECT Id FROM AcademicYear a WHERE a.AcademicYear = r.AcademicYear) AcademicYearId,
       Type, DateFrom, DateTo, Class
FROM AcademicYearRange r;

drop table AcademicYearRange;

alter table AcademicYearRange_tmp rename to AcademicYearRange;
       ")
    override this.Down() =
        base.Execute(@"
create table AcademicYearRange_tmp
(
    Id           INTEGER            not null
        constraint PK_academic_year_range
            primary key autoincrement,
    AcademicYear TEXT               not null,
    Type         INTEGER            not null,
    DateFrom     TEXT,
    DateTo       TEXT,
    Class        INTEGER default -1 not null
);

INSERT INTO AcademicYearRange_tmp (Id, AcademicYear, Type, DateFrom, DateTo, Class)
SELECT Id,
       (SELECT AcademicYear FROM AcademicYear a WHERE a.Id = r.AcademicYearId) AcademicYear,
       Type, DateFrom, DateTo, Class
FROM AcademicYearRange r;

drop table AcademicYearRange;

alter table AcademicYearRange_tmp rename to AcademicYearRange;

drop table AcademicYear;
      ")
