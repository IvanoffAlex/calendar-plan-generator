import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { Button } from 'antd';
import { Guid } from 'guid-typescript/dist/guid';
import { ScheduleItem, ScheduleItemRendererProps } from '../interfaces/calendar-plan-interfaces';
import { calendarPlanStore } from '../services/calendar-plan-store';

export const ScheduleItemRenderer = observer(({ item }: ScheduleItemRendererProps) => {
  function onDeleteItemClick (id?: Guid) {
    const newSchedules = calendarPlanStore.schedule.Schedules.filter(
      (item : ScheduleItem) => { return item.Id !== id; });
    calendarPlanStore.schedule.Schedules = newSchedules;
  }

  function onEditItemClick (id?: Guid) {
    const item : ScheduleItem | null =
      calendarPlanStore.schedule.Schedules.find(
        (item : ScheduleItem) => { return item.Id === id; }) ?? null;
    calendarPlanStore.setSelectedScheduleItem(item);
  }

  function getDayName (item: ScheduleItem) {
    switch (item.Day) {
      case 1: return 'Понеділок';
      case 2: return 'Вівторок';
      case 3: return 'Середа';
      case 4: return 'Четвер';
      case 5: return 'П\'ятниця';
      case 6: return 'Субота';
      case 0: return 'Неділя';
    }
    return '¯\\_(ツ)_/¯';
  }

  function getItemOccurence (item: ScheduleItem) {
    if (item.Period > 1) {
      return `1 раз на ${item.Period} тиждня (#${item.Offset})`;
    }
    return 'Кожного тиждня';
  }

  return <>
    <p key={Guid.create().toString()}>
      <span className={'form-label fixed-item align-right'} style={{ width: 120 }}>{ `${getDayName(item)} #${item.LessonNumber}` }</span>
      <span className='form-label fixed-item' style={{ width: 300 }}>{ `"${item.Subject}"` }</span>
      <span className='form-label fixed-item' style={{ width: 140 }}>{ `Навантаження: ${item.Load ?? 1}` }</span>
      <span className='form-label fixed-item' style={{ width: 200 }}>{ getItemOccurence(item) }</span>
      <span className='form-label'>
      <Button onClick={() => onEditItemClick(item.Id)} >📖</Button>
      </span>
      <Button danger onClick={() => onDeleteItemClick(item.Id)} >❌</Button>
    </p>
  </>;
});
