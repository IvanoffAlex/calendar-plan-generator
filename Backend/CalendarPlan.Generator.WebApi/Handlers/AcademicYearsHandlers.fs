module CalendarPlan.Generator.WebApi.Handlers.AcademicYearsHandlers

open System.Threading.Tasks
open CalendarPlan.Generator.Core
open FSharp.Control
open Microsoft.AspNetCore.Http
open Microsoft.OpenApi.Models
open Oxpecker
open Oxpecker.OpenApi
open CalendarPlan.Generator.Core.Types
open CalendarPlan.Generator.WebApi
open CalendarPlan.Generator.WebApi.ViewModels

type AcademicYearsMeta() =
    let metaSummary = "Academic Years"
    let metaTags = [| OpenApiTag(Name = "Academic Years") |]

    member this.RetrieveAcademicYears =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Retrieve all active academic years" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<ReferenceListItem<string> array>) ])

    member this.ManageRetrieveAcademicYears =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Retrieve all academic years" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<ReferenceListItem<string> array>) ])

    member this.ManageCheckAcademicYear =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Check academic year ranges" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<AcademicYear>) ])

    member this.ManageRetrieveRanges =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Get academic year ranges" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<Db.RangeModel array>) ])

    member this.ManageCreateAcademicYears =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Create academic year" metaSummary metaTags,
            requestBody = RequestBody(typeof<Db.AcademicYearModel>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<AcademicYear>) ])

    member this.ManageUpdateAcademicYears =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Update academic year" metaSummary metaTags,
            requestBody = RequestBody(typeof<Db.AcademicYearModel>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<AcademicYear>) ])

    member this.ManageUpdateRanges =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Save academic year ranges" metaSummary metaTags,
            requestBody = RequestBody(typeof<Db.RangeModel array>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<AcademicYear>) ])

let private meta = AcademicYearsMeta()

let private validationResult message (ctx: HttpContext) =
    ctx.SetStatusCode StatusCodes.Status400BadRequest
    json { StatusCode = StatusCodes.Status400BadRequest
           Message = message } ctx

let private retrieveAcademicYears isActive (ctx: HttpContext) =
    task {
        let statuses = match isActive with | Some true -> [1] | Some false -> [0] | None -> [0;1]
        let db = ctx.GetService<Db.FSharpDapperFactory>()
        let! academicYears = Db.GetAcademicYearsAsync statuses db
        let years = academicYears
                    |> Seq.map (fun x -> { Id = x.Id
                                           Name = x.AcademicYear
                                           IsActive = match x.IsActive with | 1 -> true | _ -> false })
                    |> Seq.toArray

        return! json years ctx
    }
    :> Task

let private checkAcademicYear academicYearId studentClass (ctx: HttpContext) =
    task {
        let db = ctx.GetService<Db.FSharpDapperFactory>()
        let! academicYear = Db.GetAcademicYearAsync db academicYearId (Some(studentClass))
        return json academicYear ctx
    }
    :> Task

let private retrieveAcademicYearRanges academicYearId (ctx: HttpContext) =
    task {
        let db = ctx.GetService<Db.FSharpDapperFactory>()
        let! ranges = Db.GetEditableRangesAsync db academicYearId
        return json ranges ctx
    }
    :> Task

let private createAcademicYear (ctx: HttpContext) =
    task {
        let! model = ctx.BindJson<Db.AcademicYearModel>()

        if (model.Range.From >= model.Range.To) then
            return! validationResult "Дата початку календарного року більше ніж його закінчення" ctx
        if (model.Range.To.Year - model.Range.From.Year <> 1) then
            return! validationResult "Рік закінчення календарного року повинен бути в наступному році" ctx


        let db = ctx.GetService<Db.FSharpDapperFactory>()
        let! academicYearId = Db.InsertAcademicYearAsync db model

        let! academicYear = Db.GetAcademicYearAsync db academicYearId None
        return! json academicYear ctx
    }
    :> Task

let private updateAcademicYear academicYearId (ctx: HttpContext) =
    task {
        let! model = ctx.BindJson<Db.AcademicYearModel>()
        let academicYear = $"%i{model.Range.From.Year}-%i{model.Range.To.Year}"

        let db = ctx.GetService<Db.FSharpDapperFactory>()

        let! old = Db.GetAcademicYearAsync db academicYearId None
        if (old.AcademicYear <> academicYear) then
            return! validationResult "Роки календарного року повинені бути незмінними" ctx

        Db.UpdateAcademicYearAsync db academicYearId model |> ignore

        let! academicYear = Db.GetAcademicYearAsync db academicYearId None
        return! json academicYear ctx
    }
    :> Task

let private saveAcademicYearRanges academicYearId (ctx: HttpContext) =
    task {
        let! model = ctx.BindJson<Db.RangeModel array>()

        let db = ctx.GetService<Db.FSharpDapperFactory>()
        Db.SaveAcademicYearRangesAsync db academicYearId model |> ignore

        let! academicYear = Db.GetAcademicYearAsync db academicYearId None
        return! json academicYear ctx
    }
    :> Task

let SetupAcademicYearsEndpoints() =
    GET [ route "/academic-years" <| retrieveAcademicYears (Some(true)) |> addOpenApi(meta.RetrieveAcademicYears) ]

let SetupManageAcademicYearsEndpoints() =
    subRoute "/academic-years" [
        GET [ route "/" <| retrieveAcademicYears None |> addOpenApi(meta.ManageRetrieveAcademicYears)
              routef "/{%i}/ranges" <| retrieveAcademicYearRanges |> addOpenApi(meta.ManageRetrieveRanges)
              routef "/{%i}/classes/{%i}" <| checkAcademicYear |> addOpenApi(meta.ManageCheckAcademicYear)
        ]
        PUT [ routef "/{%i}" <| updateAcademicYear |> addOpenApi(meta.ManageUpdateAcademicYears)
              routef "/{%i}/ranges" <| saveAcademicYearRanges |> addOpenApi(meta.ManageUpdateRanges)
        ]
        POST [ route "/" <| createAcademicYear |> addOpenApi(meta.ManageCreateAcademicYears)]
    ]
