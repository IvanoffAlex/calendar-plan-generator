export interface SuccessAction {
    (message: string): void;
}

export interface ErrorAction {
    (event: Error): void;
}

export interface LoadingComponentProps {
    isLoading: boolean
}
