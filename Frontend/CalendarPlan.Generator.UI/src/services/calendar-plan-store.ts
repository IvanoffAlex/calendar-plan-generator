import { makeObservable, observable, action, computed } from 'mobx';
import { AcademicYearListItem, DayOfWeek, Schedule, ScheduleItem } from '../interfaces/calendar-plan-interfaces';
import { Guid } from 'guid-typescript/dist/guid';

export class CalendarPlanStore {
  schedule : Schedule = null as unknown as Schedule;

  selectedAcademicYear = ''

  selectedScheduleItem : ScheduleItem | null = null;
  errors: string[] = [];

  constructor () {
    makeObservable(this, {
      schedule: observable,
      selectedScheduleItem: observable,
      selectedAcademicYear: observable,
      errors: observable,

      isScheduleItemEditable: computed,
      isOffsetHidden: computed,
      isDisabledSelectedYear: computed,
      isDataInvalid: computed,
      academicYear: computed,
      academicYearId: computed,
      studentClass: computed,
      scheduleTemplate: computed,
      isValidForm: computed,

      setAcademicYear: action,
      setStudentClass: action,

      setSchedule: action,
      resetSchedule: action,

      setSelectedScheduleItem: action,
      setSelectedItemSubject: action,
      setSelectedItemLessonNumber: action,
      setSelectedItemDay: action,
      setSelectedItemPeriod: action,
      setSelectedItemOffset: action,
      applyItemToSchedule: action,

      clearErrors: action,
      addErrors: action
    });
  }

  get isDisabledSelectedYear () : boolean {
    return this.academicYear === '';
  }

  get isScheduleItemEditable () : boolean {
    return this.selectedScheduleItem?.Id === undefined;
  }

  get isOffsetHidden () : boolean {
    if (!this.selectedScheduleItem) {
      return false;
    }
    return this.selectedScheduleItem.Period === 1;
  }

  get isValidForm () : boolean {
    return this.errors.length === 0;
  }

  get isDataInvalid () : boolean {
    return this.schedule?.Schedules?.length === 0 ||
          this.academicYear === '';
  }

  get scheduleTemplate () : string {
    return this.schedule ? JSON.stringify(this.schedule, null, 2) : '';
  }

  get academicYear () : string {
    return calendarPlanStore.selectedAcademicYear;
  }

  get academicYearId () : number {
    return calendarPlanStore.schedule?.AcademicYearId;
  }

  get studentClass () : number | undefined {
    return calendarPlanStore.schedule?.StudentClass;
  }

  setStudentClass (value: number | null | undefined): void {
    this.schedule.StudentClass = value != null ? value : undefined;
  }

  setAcademicYear (academicYear: AcademicYearListItem): void {
    if (!this.schedule) {
      this.schedule = { AcademicYearId: academicYear.Id, Schedules: [] };
      this.selectedAcademicYear = academicYear.Name;
    } else {
      this.schedule.AcademicYearId = academicYear.Id;
      this.selectedAcademicYear = academicYear.Name;
    }
  }

  setSchedule (value: string): void {
    const parsedPlan = JSON.parse(value) as Schedule;
    parsedPlan.Schedules.forEach(item => { item.Id ??= Guid.create(); });
    this.schedule = parsedPlan;
  }

  resetSchedule (): void {
    this.schedule = { AcademicYearId: this.academicYearId, Schedules: [] };
    this.selectedAcademicYear = this.academicYear;
  }

  setSelectedScheduleItem (value: ScheduleItem | null): void {
    this.selectedScheduleItem = value;
    this.clearErrors();
  }

  setSelectedItemSubject (value: string): void {
    if (this.selectedScheduleItem) {
      this.selectedScheduleItem.Subject = value;
    }
  }

  setSelectedItemLessonNumber (value: number): void {
    if (this.selectedScheduleItem) {
      this.selectedScheduleItem.LessonNumber = value !== null ? value : 1;
    }
  }

  setSelectedItemDay (value: DayOfWeek): void {
    if (this.selectedScheduleItem) {
      this.selectedScheduleItem.Day = value;
    }
  }

  setSelectedItemLoad (value: number): void {
    if (this.selectedScheduleItem) {
      this.selectedScheduleItem.Load = value;
    }
  }

  setSelectedItemPeriod (value: number): void {
    if (this.selectedScheduleItem) {
      this.selectedScheduleItem.Period = value;
      if (this.selectedScheduleItem.Offset > value) {
        this.setSelectedItemOffset(value);
      }
    }
  }

  setSelectedItemOffset (value: number): void {
    if (this.selectedScheduleItem) {
      this.selectedScheduleItem.Offset = value !== null ? value : 1;
    }
  }

  applyItemToSchedule (item : ScheduleItem | null) : void {
    if (!this.validate(item) || !item) {
      return;
    }

    const edit = calendarPlanStore.schedule.Schedules.find(i => i.Id === item.Id);
    if (edit) {
      Object.assign(edit, item);
    } else {
      calendarPlanStore.schedule.Schedules.push(item);
    }

    calendarPlanStore.schedule.Schedules.sort((a, b) => this.scheduleComparer(a, b));

    calendarPlanStore.setSelectedScheduleItem(null);
  }

  clearErrors (): void {
    this.errors = [];
  }

  addErrors (error : string): void {
    this.errors.push(error);
  }

  validate (item : ScheduleItem | null) : boolean {
    // validation -> make logic based on rules
    calendarPlanStore.clearErrors();
    if (!item) {
      calendarPlanStore.addErrors('Фатальна помилка');
      return false;
    }
    if (typeof item?.Subject === 'undefined' || !item?.Subject) {
      calendarPlanStore.addErrors('Будь ласка, введіть назву предмета.');
      return false;
    }
    const found = calendarPlanStore.schedule
      .Schedules.find(i =>
        i.Id !== item.Id &&
        i.Day === item.Day &&
        i.LessonNumber === item.LessonNumber &&
        (i.Offset === item.Offset || i.Period !== item.Period));
    if (found) {
      calendarPlanStore.addErrors('Цей час вже зарезервовано');
      return false;
    }
    return true;
  }

  scheduleComparer (a : ScheduleItem, b : ScheduleItem) : number {
    if (a.Day === b.Day && a.LessonNumber === b.LessonNumber) {
      return a.Offset - b.Offset;
    }
    if (a.Day === b.Day) {
      return a.LessonNumber - b.LessonNumber;
    }
    return a.Day - b.Day;
  }
}

export const calendarPlanStore = new CalendarPlanStore();
