﻿namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20210911232635L, "Seed AcademicYearRange Table")>]
type _20210911232635_SeedAcademicYearRangeTable() =
    inherit Migration()

    let upScript = @"
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (1, '2021-2022', 0, '2021-09-01', '2022-05-31');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (2, '2021-2022', 1, '2021-10-14', '2021-10-14');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (3, '2021-2022', 1, '2021-12-25', '2021-12-25');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (4, '2021-2022', 1, '2021-12-27', '2021-12-27');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (5, '2021-2022', 1, '2022-01-01', '2022-01-01');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (6, '2021-2022', 1, '2022-01-07', '2022-01-07');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (7, '2021-2022', 1, '2022-03-08', '2022-03-08');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (8, '2021-2022', 2, '2021-10-15', '2021-10-23');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (9, '2021-2022', 3, '2021-10-25', '2021-10-31');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (10, '2021-2022', 3, '2021-12-25', '2022-01-09');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (11, '2021-2022', 3, '2022-02-28', '2022-03-14');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (12, '2021-2022', 3, '2022-03-14', '2022-03-27');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (13, '2022-2023', 0, '2022-09-01', '2023-05-31');"
    override this.Up() =
        base.Execute(upScript)
    override this.Down() =
        base.Execute(@"delete from AcademicYearRange where 1=1")
