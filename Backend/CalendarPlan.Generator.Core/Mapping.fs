module CalendarPlan.Generator.Core.Mapping
open System
open CalendarPlan.Generator.Core.Types

let private getAllMonthsInRange range =
    let initialValue = DateTime(range.From.Year, range.From.Month, 1)
    Seq.initInfinite int
    |> Seq.map initialValue.AddMonths
    |> Seq.takeWhile (fun data -> data <= range.To)

let private intersectExcelData (subject: SubjectEvents) excelData =
    excelData
    |> Seq.filter(fun (title, _) -> title = subject.Title)
    |> Seq.map snd
    |> Seq.concat
    |> Seq.groupBy (fun x -> x.SectionIndex, x.SectionTitle)
    |> Seq.map (fun ((x,y),z) -> (x,y,z))

let ToExcelItems academicYear (subjects : SubjectEvents list) =
    let semesters = seq [1..2]
    semesters
    |> Seq.collect (fun semester ->
        subjects
        |> List.map (fun subject ->
        {
             Subject = subject.Title
             Semester = semester
             Indexes = subject.Events
                 |> Seq.filter(fun event -> event.Date.Year - academicYear.Range.From.Year = semester - 1)
                 |> Seq.map (_.Index)
        }))

let ToMonthMReport month (subjects: SubjectEvents list) =
    let calculatedMonthHours = subjects |> List.toSeq
    let events = calculatedMonthHours
              |> Seq.filter (fun x -> (x.Events |> Seq.length) > 0)
              |> Seq.tryHead
    let year = match events with | Some(x) -> (x.Events |> Seq.head).Date.Year | None -> 1

    dict [ (DateTime(year, month, 1), calculatedMonthHours) ]

let ToMonthsReport academicYear (subjects: SubjectEvents list) =
    let allMonths = getAllMonthsInRange academicYear.Range
    allMonths
    |> Seq.collect (fun month ->
        subjects
        |> List.map (fun subject ->
            (month, { Title = subject.Title
                      Load = subject.Load
                      Events = subject.Events
                            |> Seq.filter(fun event ->
                              event.Date.Year = month.Year &&
                              event.Date.Month = month.Month) })))
    |> Seq.groupBy fst
    |> Seq.map(fun (key, values) -> (key, values |> Seq.map snd))
    |> dict

let ToCalendarPlanDocument (subjects: SubjectEvents list) =
    { Subjects =
        subjects
        |> List.map(fun subject ->
            { Title = subject.Title
              Load = subject.Load
              IsSectionVisible = false
              Sections = [| {
                    Index = 1
                    Title = String.Empty
                    Lessons = subject.Events
                              |> Seq.map (fun event ->
                                  { Index = event.Index
                                    Title = String.Empty
                                    Load = event.Load
                                    Date = event.Date })
                              |> Seq.toArray
            } |] })
        |> List.toArray
    }

let ToCalendarPlanDocumentFromExcelData subjects excelData =
    { Subjects =
        subjects
        |> Seq.map (fun subject ->
            (let sections = intersectExcelData subject excelData
            { Title = subject.Title
              Load = subject.Load
              IsSectionVisible = sections |> Seq.length > 1
              Sections = sections |> Seq.map (fun (index, title, lessons) ->
                  { Index = index
                    Title = title
                    Lessons = lessons |> Seq.map (fun lesson ->
                        let event = subject.Events |> Seq.find (fun x -> x.Index = lesson.LessonIndex)
                        { Index  = lesson.LessonIndex
                          Title = lesson.LessonTitle
                          Load = event.Load
                          Date = event.Date })
                          |> Seq.sortBy (_.Index)
                          |> Seq.toArray
                    })
                    |> Seq.sortBy (_.Index)
                    |> Seq.toArray }
            ))
        |> Seq.toArray }
