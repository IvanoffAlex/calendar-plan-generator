/* eslint-disable @typescript-eslint/no-misused-promises */
import * as React from 'react';
import { useState } from 'react';
import { observer } from 'mobx-react-lite';

import { Col, Row, Select, Button, Input, Modal, InputNumber } from 'antd';
import FileDownload from 'js-file-download';

import { MonthReportOptions } from '../interfaces/calendar-plan-interfaces';
import { calendarPlanStore } from '../services/calendar-plan-store';
import { calendarPlanDataService } from '../services/calendar-plan-data-service';
import { LoadingComponentProps } from '../interfaces/generic-interfaces';

export const MonthReportArea = observer(({ isLoading }: LoadingComponentProps) => {
  const { Option } = Select;
  const [monthReport, setMonthReport] = useState<number>(0);
  const [monthOptions, setMonthOptions] = useState<MonthReportOptions>({ Teacher: '', Student: '', Class: 1 });
  const [showMonthDialog, setShowMonthDialog] = useState(false);

  function isGenerationAvailable () {
    return !calendarPlanStore.isDataInvalid &&
      !isLoading;
  }

  function isMonthReportAvailable () {
    return isGenerationAvailable() &&
      calendarPlanStore.studentClass &&
      monthOptions.Teacher.length > 0 &&
      monthOptions.Student.length > 0;
  }

  async function onApply () {
    const schedule = { Meta: monthOptions, Schedule: calendarPlanStore.schedule };
    let filename = monthReport === 0
      ? calendarPlanDataService.buildCalendarPlanFilename(calendarPlanStore.academicYear, 'all-months-report', 'docx')
      : calendarPlanDataService.buildCalendarPlanFilename(calendarPlanStore.academicYear, 'month-report', 'docx');
    const monthReportPromise = monthReport === 0
      ? async () => await calendarPlanDataService.allMonthsReport(schedule)
      : async () => await calendarPlanDataService.monthReport(monthReport, schedule);
    await monthReportPromise().then(response => {
      filename = response.headers['content-disposition']?.split('filename=')[1] ?? filename;
      FileDownload(response.data, filename);
    });
  }

  return <>
    <Button type={'default'} onClick={() => setShowMonthDialog(true)} disabled={!isGenerationAvailable()}>Щомісячні звіти</Button>
    <Modal title={'Місячні звіти'} width={550}
      footer={[
        <Button key="back" onClick={() => setShowMonthDialog(false)}>
          Відміна
        </Button>,
        <Button key="submit" type="primary" onClick={onApply} disabled={!isMonthReportAvailable()}>
          Сгенерувати {(!isMonthReportAvailable() || monthReport === 0) ? 'усі місячні звіти' : 'місячний звіт' }
        </Button>
      ]}
      open={showMonthDialog}
      onCancel={() => setShowMonthDialog(false)}>
      <Row gutter={12} className={'content-layout'} >
        <Col span={10} className={'align-right'}>ПІБ Вчителя<span className={'required'}>*</span>:</Col>
        <Col span={10}><Input placeholder={'ПІБ Вчителя'} style={{ width: 200 }}
          value={monthOptions.Teacher}
          onChange={(e => {
            const options = { ...monthOptions };
            options.Teacher = e.target.value;
            setMonthOptions(options);
          })} /></Col>
      </Row>
      <Row gutter={12} className={'content-layout'} >
        <Col span={10} className={'align-right'}>ПІБ Учня<span className={'required'}>*</span>:</Col>
        <Col span={10}><Input placeholder={'ПІБ Учня'} style={{ width: 200 }}
          value={monthOptions.Student}
          onChange={(e => {
            const options = { ...monthOptions };
            options.Student = e.target.value;
            setMonthOptions(options);
          })} /></Col>
      </Row>
      <Row gutter={12} className={'content-layout'} >
        <Col span={10} className={'align-right'}>Клас<span className={'required'}>*</span>:</Col>
        <Col span={10}><InputNumber min={1} max={12} placeholder={'Клас'} style={{ width: 70 }}
            value={calendarPlanStore.studentClass}
            onChange={((value) => calendarPlanStore.setStudentClass(value))}
            /></Col>
      </Row>
      <Row gutter={12} className={'content-layout'} >
        <Col span={10} className={'align-right'}>Місяць (необов&apos;язково):</Col>
        <Col span={10}>
          <Select style={{ width: 200 }} disabled={!isGenerationAvailable()}
            value={monthReport}
            onChange={((value) => setMonthReport(value))}>
            <Option value={0}>(Виберіть місяць)</Option>
            <Option value={1}>Січень</Option>
            <Option value={2}>Лютий</Option>
            <Option value={3}>Березень</Option>
            <Option value={4}>Квітень</Option>
            <Option value={5}>Травень</Option>
            <Option value={6}>Червень</Option>
            <Option value={7}>Липень</Option>
            <Option value={8}>Серпень</Option>
            <Option value={9}>Вересень</Option>
            <Option value={10}>Жовтень</Option>
            <Option value={11}>Листопад</Option>
            <Option value={12}>Грудень</Option>
          </Select>
          </Col>
      </Row>
    </Modal>
  </>;
});
