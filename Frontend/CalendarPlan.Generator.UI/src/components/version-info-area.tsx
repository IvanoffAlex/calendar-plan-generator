/* eslint-disable @typescript-eslint/no-misused-promises */
import * as React from 'react';
import { useState } from 'react';

import { Button, Modal, Row } from 'antd';

import { calendarPlanDataService } from './../services/calendar-plan-data-service';

import { name, version } from './../../package.json';
import { VersionInfo } from './../interfaces/calendar-plan-interfaces';

export const VersionInfoArea = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [message, setMessage] = useState<VersionInfo | null>(null);

  const handleClose = () => {
    setIsModalOpen(false);
  };

  async function getVersionInfo () {
    const result = await calendarPlanDataService.getVersion();
    setMessage(result.data);
    setIsModalOpen(true);
  }

  return <>
        <Button onClick={() => getVersionInfo()} type='link'>Calendar Plan Generator</Button>
        <Modal title="About" footer={null} open={isModalOpen} onCancel={handleClose}>
            <Row><strong>Modules:</strong></Row>
            <Row>{name}: {version}</Row>
            {message?.Assemblies.map((assembly, i) => <Row key={i}>{assembly.AssemblyName}: {assembly.Version}</Row>)}
            <br />
            <Row><strong>Runtime:</strong></Row>
            <Row>{message?.Runtime}</Row>
            <br />
            <br />
            <Row>© by Stanislav Poliakov</Row>
        </Modal>
    </>;
};
