module CalendarPlan.Generator.WebApi.Program

open System
open System.IO
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting

let CreateDefaultBuilder args =
    Host
        .CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(fun webHostBuilder ->
            let contentRoot = AppContext.BaseDirectory
            let webRoot = Path.Combine(contentRoot, "ui")

            webHostBuilder
                .UseContentRoot(contentRoot)
                .UseWebRoot(webRoot)
                .Configure(Startup.ConfigureApp)
                .ConfigureServices(Startup.ConfigureServices)
                .ConfigureLogging(Startup.ConfigureLogging)
            |> ignore)

[<EntryPoint>]
let main args =
    Console.OutputEncoding <- System.Text.Encoding.UTF8
    let host = (CreateDefaultBuilder args).Build()
    Startup.WithMigration host.Services
    host.Run()

    Startup.ExitCode
