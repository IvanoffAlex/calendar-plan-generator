/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-floating-promises */
import * as React from 'react';
import FileDownload from 'js-file-download';
import { Button, Layout, List, Space, Upload, UploadFile, message } from 'antd';
import { calendarPlanStore } from '../services/calendar-plan-store';
import { LoadingComponentProps, SuccessAction } from '../interfaces/generic-interfaces';
import { calendarPlanDataService } from '../services/calendar-plan-data-service';
import { useState } from 'react';
import { observer } from 'mobx-react-lite';

export const ExcelPlanProcessor = observer(({ isLoading }: LoadingComponentProps) => {
  const [fileListMap, setFileListMap] = useState(new Map<string, ArrayBuffer>());

  function isGenerationAvailable () {
    return !calendarPlanStore.isDataInvalid &&
      !isLoading;
  }

  function cloneFilesToUpload () : Map<string, ArrayBuffer> {
    const clone = new Map<string, ArrayBuffer>();
    fileListMap.forEach((value: ArrayBuffer, key: string) => {
      clone.set(key, value);
    });
    return clone;
  }

  const excelUpload = {
    showUploadList: false,
    multiple: true,
    beforeUpload: (file : File) => {
      if (fileListMap.has(file.name)) {
        return Upload.LIST_IGNORE;
      }
      const isValidFile =
        file.type === 'application/json' ||
        file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      if (!isValidFile) {
        message.error(`${file.name} повинен бути в форматі JSON або xlsx`);
      }
      return isValidFile || Upload.LIST_IGNORE;
    },
    onRemove: (file: UploadFile) => {
      fileListMap.delete(file.name);
      setFileListMap(cloneFilesToUpload());
    },
    customRequest: (options) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const { onError } = options;
      const onSuccess = options.onSuccess as SuccessAction;
      const file = options.file as File;
      const reader = new FileReader();
      reader.onload = (e) => {
        try {
          if (file.name.includes('.json')) {
            calendarPlanStore.setSchedule(e.target?.result as string);
            fileListMap.set(file.name, new ArrayBuffer(0));
          } else {
            fileListMap.set(file.name, e.target?.result as ArrayBuffer);
          }
          setFileListMap(cloneFilesToUpload());
          message.success(`${file.name} успішно завантажений`);
          onSuccess('Ok');
        } catch (error) {
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          message.error(`${file.name} має некорректний формат. Помилка: ${error.message}`, 10);
          // eslint-disable-next-line @typescript-eslint/no-unsafe-call
          onError({ event: error });
        }
      };
      if (file.name.includes('.json')) {
        reader.readAsText(file as Blob);
      } else {
        reader.readAsArrayBuffer(file as Blob);
      }
    }
  };

  async function transformExcelFilesToWord () {
    return calendarPlanDataService.transformExcelFilesToWord(calendarPlanStore.schedule, fileListMap)
      .then(response => {
        let filename = calendarPlanDataService.buildCalendarPlanFilename(calendarPlanStore.academicYear, 'calendar-plan', 'docx');
        filename = response.headers['content-disposition']?.split('filename=')[1] ?? filename;
        FileDownload(response.data, filename);
      });
  }

  function resetExcelUpload () {
    fileListMap.clear();
    setFileListMap(cloneFilesToUpload());
  }

  return <Layout className={'content-layout'}>
        <Space size={'large'}>
            <Upload {...excelUpload}>
                <Button>Завантажити Excel файли і файл конфігурації</Button>
            </Upload>
        </Space>
        <Space size={'large'} className="content-layout-top">
        <List
              locale={ { emptyText: 'Файли на трансформацію незавантажені' } }
              size="small"
              dataSource={[...fileListMap.keys()]}
              renderItem={(item) => <List.Item>{item.endsWith('.json') ? '⚒️' : '📗'} {item}</List.Item>}
            />
        </Space>
        <Space size={'large'} className="content-layout-top">
            <Button onClick={() => transformExcelFilesToWord()} disabled={!isGenerationAvailable() || fileListMap.size === 0}>Трансформувати Excel файли в Календарний план</Button>
        </Space>
        <Space size={'large'} className="content-layout-top">
            <Button onClick={() => resetExcelUpload()} disabled={!isGenerationAvailable() || fileListMap.size === 0}>Очистити</Button>
        </Space>
    </Layout>;
});
