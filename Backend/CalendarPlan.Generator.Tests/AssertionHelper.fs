module CalendarPlan.Generator.Tests.AssertionHelper
open System.Collections.Generic
open System.IO
open System.IO.Compression
open System.Net.Http
open System.Text
open System.Text.Json
open System.Threading
open CalendarPlan.Generator.Core.Types
open CalendarPlan.Generator.WebApi
open DocumentFormat.OpenXml
open DocumentFormat.OpenXml.Packaging
open FsUnit.Xunit

let private toSubjectTitle (x : KeyValuePair<string, SubjectEvents>) = (x.Key, x.Value.Load)

let private getEvents subjectEvents title =
    let single = subjectEvents |> List.filter (fun (x : SubjectEvents) -> x.Title = title) |> List.exactlyOne
    single.Events

let GetMessageAsync (response : HttpResponseMessage) =
    task {
        return! response.Content.ReadAsStringAsync CancellationToken.None
    }

let FromJson<'T> (json : string) = JsonSerializer.Deserialize<'T> (json, JsonSettings.DeserializationOptions)
let ToJson obj = JsonSerializer.Serialize (obj, JsonSettings.DeserializationOptions)

let GetObjectAsync<'T> (response: HttpResponseMessage) =
    task {
        let! message = GetMessageAsync response
        return FromJson<'T> message
    }

let RetrieveExpectedTestAsset<'T> name =
    let json = TestAssets.RetrieveEmbeddedResourceContent name
    FromJson<'T> json

let AssertSubjectEvents (actual: SubjectEvents seq) expectedEvents expectedSubjects =
    let assertResult = actual |> Seq.map (fun x -> x.Title, x) |> dict
    assertResult |> should haveCount (expectedSubjects |> Seq.length)
    assertResult |> Seq.map toSubjectTitle |> should equalSeq expectedSubjects
    for entry in assertResult do
        let expectedEvents = getEvents expectedEvents entry.Key
        entry.Value.Events |> should equalSeq expectedEvents

let Assert<'T> actual expected =
    let json = ToJson actual
    let result = FromJson<'T> json
    result |> should equal expected

let AssertHttpContentAsWordDocument (content: HttpContent) filenamePart =
    content.Headers.ContentDisposition |> should startWith $"attachment; filename=%s{filenamePart}."
    content.Headers.ContentDisposition |> should endWith ".docx"
    content.Headers.ContentType |> string |> should equal "application/octet-stream"
    content.Headers.ContentLength |> should not' (be null)

    use stream = new MemoryStream()
    content.ReadAsStream().CopyTo stream
    stream.Seek(0L, SeekOrigin.Begin) |> ignore
    stream.Length |> should equal content.Headers.ContentLength

    let wordDocument = WordprocessingDocument.Open(stream, false)
    wordDocument.DocumentType |> should equal WordprocessingDocumentType.Document

let AssertHttpContentAsZip (content: HttpContent) filenamePart expectedSchedule =
    content.Headers.ContentDisposition |> should startWith $"attachment; filename=%s{filenamePart}."
    content.Headers.ContentDisposition |> should endWith ".zip"
    content.Headers.ContentType |> string |> should equal "application/octet-stream"
    content.Headers.ContentLength |> should not' (be null)

    use stream = new MemoryStream()
    content.ReadAsStream().CopyTo stream
    stream.Seek(0L, SeekOrigin.Begin) |> ignore
    stream.Length |> should equal content.Headers.ContentLength

    use zip = new ZipArchive(stream, ZipArchiveMode.Read)
    zip.Entries |> should haveCount 5

    let scheduleEntry = zip.Entries |> Seq.filter (fun x -> x.Name.EndsWith ".json") |> Seq.exactlyOne
    let actualSchedulePlan = Encoding.UTF8.GetString (TestAssets.ToBytes scheduleEntry)
    actualSchedulePlan |> should equal expectedSchedule

    zip.Entries |> Seq.filter (fun x -> x.Name.EndsWith ".xlsx") |> Seq.toArray |> should haveLength 4
    let excelEntry = zip.Entries |> Seq.filter (fun x -> x.Name.EndsWith ".xlsx") |> Seq.head
    use excelStream = new MemoryStream(TestAssets.ToBytes excelEntry)
    let excelDocument = SpreadsheetDocument.Open(excelStream, false)
    excelDocument.DocumentType |> should equal SpreadsheetDocumentType.Workbook
