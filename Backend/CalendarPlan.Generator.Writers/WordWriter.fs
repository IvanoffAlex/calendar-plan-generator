﻿module CalendarPlan.Generator.Writers.WordWriter
open System.IO
open DocumentFormat.OpenXml
open DocumentFormat.OpenXml.Packaging
open DocumentFormat.OpenXml.Wordprocessing

type WordCellMetadata =
    { Text: string
      Width: int
      Justification: EnumValue<JustificationValues> }

let private constructDocument (wordDocument : WordprocessingDocument) =
    let mainPart = wordDocument.AddMainDocumentPart()
    let document = Document()
    document.Body <- Body()
    mainPart.Document <- document
    wordDocument

let private createTextParagraph text justificationValue =
    let text = Text(text)
    text.Space <- EnumValue<SpaceProcessingModeValues >(SpaceProcessingModeValues.Preserve)
    let run = Run()
    run.AppendChild text |> ignore
    let paragraph = Paragraph() :> OpenXmlElement
    let properties = ParagraphProperties()
    let justification = Justification()
    justification.Val <- justificationValue
    properties.AppendChild justification |> ignore
    paragraph.Append(properties, run)
    paragraph

let private createCell cellInfo =
    let paragraph =
        createTextParagraph cellInfo.Text cellInfo.Justification

    let cellProperties = TableCellProperties()
    let cellWidth = TableCellWidth()
    cellWidth.Type <- EnumValue<TableWidthUnitValues>(TableWidthUnitValues.Dxa)
    cellWidth.Width <- StringValue $"%i{cellInfo.Width}"
    cellProperties.AppendChild cellWidth |> ignore

    let cell = TableCell() :> OpenXmlElement
    cell.Append(cellProperties, paragraph)
    cell

let AppendToDocument (document: Document) element =
    document.Body.AppendChild element |> ignore

let WriteBreakPage (document: Document) =
    let pageBreak = Break()
    pageBreak.Type <- EnumValue<BreakValues>(BreakValues.Page)
    let run = Run()
    run.AppendChild pageBreak |> ignore
    let paragraph = Paragraph()
    paragraph.AppendChild run |> ignore
    AppendToDocument document paragraph

let WriteParagraphText (document: Document) text justificationValue =
    let paragraph = createTextParagraph text justificationValue
    AppendToDocument document paragraph

let WriteParagraphTextLines (document: Document) lines =
    lines |> Seq.iter (fun (text, justificationValue) ->
            let paragraph = createTextParagraph text justificationValue
            AppendToDocument document paragraph)

let MergeCells cells =
    let merge (element: OpenXmlElement) enum =
        let cell = downcast element : TableCell
        let h = HorizontalMerge()
        h.Val <- enum
        cell.TableCellProperties.HorizontalMerge <- h

    merge (cells |> List.head) (EnumValue<MergedCellValues>(MergedCellValues.Restart))
    cells
    |> List.tail
    |> List.iter (fun cell -> merge cell (EnumValue<MergedCellValues>(MergedCellValues.Continue)))

let CreateRow cells =
    let row = TableRow() :> OpenXmlElement
    let rowCells =
        cells
        |> List.map createCell
    row.Append rowCells
    (row, rowCells)

let CreateTable headers =
    let table = Table()

    let properties = TableProperties()

    let tableWidth = TableWidth()
    tableWidth.Type <- EnumValue<TableWidthUnitValues>(TableWidthUnitValues.Pct)
    tableWidth.Width <- StringValue("5000")
    properties.AppendChild tableWidth |> ignore

    let tableBorders = TableBorders()
    let borders =
        [ TopBorder() :> BorderType
          BottomBorder() :> BorderType
          LeftBorder() :> BorderType
          RightBorder() :> BorderType
          InsideHorizontalBorder() :> BorderType
          InsideVerticalBorder() :> BorderType ]
        |> List.map
            (fun border ->
                border.Val <- EnumValue<BorderValues>(BorderValues.Single)
                border.Size <- UInt32Value(1u)
                border :> OpenXmlElement)
    tableBorders.Append borders
    properties.AppendChild tableBorders |> ignore

    let layout = TableLayout()
    layout.Type <- EnumValue<TableLayoutValues>(TableLayoutValues.Fixed)
    properties.TableLayout <- layout

    table.AppendChild properties |> ignore

    let row,_ = CreateRow headers
    table.AppendChild row |> ignore
    table

let CreateWordDocumentInStream (stream : Stream) =
    let wordDocument = WordprocessingDocument.Create(stream, WordprocessingDocumentType.Document)
    constructDocument wordDocument
