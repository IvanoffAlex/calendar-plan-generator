module CalendarPlan.Generator.WebApi.HttpContextUtility
open System.IO
open System.IO.Compression
open System.Net.Mime
open System.Security.Claims
open System.Text
open System.Text.Json
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Configuration
open Oxpecker

let private compileZipEntry zip filename inputStream =
    let zipArchive: ZipArchive = zip
    let inputStream: Stream = inputStream
    inputStream.Seek(0L, SeekOrigin.Begin) |> ignore
    let zipEntry = zipArchive.CreateEntry filename
    use entryStream = zipEntry.Open()
    inputStream.CopyTo(entryStream)
    inputStream.Dispose()

let MimeTypeJson = "application/json"
let MimeTypeOctetStream = "application/octet-stream"

let CreateZipArchive addZipEntries =
    use zipStream = new MemoryStream()
    use zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true)
    let addZipEntry (x: string, y: Stream) = compileZipEntry zip x y

    addZipEntries addZipEntry

    zip.Dispose()
    zipStream.Dispose()
    zipStream.ToArray()

let ReadFormBody<'T> ctx key =
    let context: HttpContext = ctx
    let json = context.TryGetFormValue key |> Option.get
    JsonSerializer.Deserialize<'T> (json, JsonSettings.DeserializationOptions)

let MapParallel seq readFile =
    seq
        |> Seq.toArray
        |> Array.Parallel.map readFile
        |> Array.toList
        |> List.toSeq

let ToBytes obj =
    let json = JsonSerializer.Serialize(obj, JsonSettings.DeserializationOptions)
    Encoding.UTF8.GetBytes json

let SetOutputStreamHttpHeader alias extension ctx =
    let context: HttpContext = ctx
    let filename = Path.ChangeExtension (Path.GetRandomFileName(), extension)
    let disposition = ContentDisposition()
    disposition.FileName <- $"%s{alias}.%s{filename}"
    disposition.Inline <- false

    context.SetHttpHeader ("Content-Disposition", disposition.ToString())
    context.SetHttpHeader ("Access-Control-Expose-Headers", "Content-Disposition")
    context.SetHttpHeader ("X-Content-Type-Options", "nosniff")
    context.SetHttpHeader ("Content-Type", "application/octet-stream")

let IsSupervisors (ctx: HttpContext) (user: ClaimsPrincipal) =
    let configuration = ctx.GetService<IConfiguration>()
    let supervisorsEmails = configuration.GetSection("Authorization:Supervisors").Get<string[]>() |> Set.ofArray
    let emailClaim = "identity/claims/emailaddress"
    user.HasClaim (fun claim ->
        claim.Type.EndsWith(emailClaim)
        && supervisorsEmails |> Set.contains claim.Value )
