/* eslint-disable @typescript-eslint/no-misused-promises */
import * as React from 'react';
import { useEffect, useState } from 'react';

import { Button, Space } from 'antd';

import { calendarPlanDataService } from '../services/calendar-plan-data-service';

import { AccessRight } from '../interfaces/calendar-plan-interfaces';
import { appSettings } from '../app-settings';

export const AuthenticationArea = () => {
  const [accessRight, setAccessRight] = useState({} as AccessRight);
  useEffect(() => {
    const getAccessRight = async () => {
      const result = await calendarPlanDataService.getAccessRight();
      setAccessRight(result.data);
    };

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    getAccessRight();
  }, []);

  return <>
    <Space direction={'vertical'} size={'large'} className={'user-info-layout'}>
      { accessRight.IsAuthenticated
        ? <>
          <Space>
            <Button type="default" href={`${appSettings.BaseWebApiUrl}/auth/logoff`}>Вийти</Button>
            Привіт, {accessRight.Name}</Space>
            { accessRight.CanManage
              ? <Button ghost type="primary" onClick={() => calendarPlanDataService.getAccessRight()}>⚙️ Налаштувати календарний рік</Button>
              : null
            }
        </>
        : <Button type="default" href={`${appSettings.BaseWebApiUrl}/auth/auth-google`}>Зайти як адміністратор</Button>
      }
    </Space>
    </>;
};
