namespace CalendarPlan.Generator.Tests
open System
open System.Net
open CalendarPlan.Generator
open CalendarPlan.Generator.Core.Types
open Xunit
open FsUnit.Xunit

type ApiTests() =
    let testServer = TestAssets.CreateTestServer

    [<Fact>]
    member _.``Retrieve web client url by root`` () =
        task {
            use client = testServer.CreateClient()

            let! response = client.GetAsync "/"

            response.StatusCode |> should equal HttpStatusCode.Found
            response.Headers.Location.AbsoluteUri |> should equal TestAssets.DummyClientUrl
        }

    [<Fact>]
    member _.``Receive pong from ping`` () =
        task {
            use client = testServer.CreateClient()

            let! response = client.GetAsync "/api/v2/ping"

            let! result = AssertionHelper.GetMessageAsync response
            response.StatusCode |> should equal HttpStatusCode.OK
            result |> should equal "pong"
        }

    [<Fact>]
    member _.``Get version`` () =
        task {
            use client = testServer.CreateClient()
            let expectedAssemblies = seq [
                "CalendarPlan.Generator.Core"
                "CalendarPlan.Generator.Migrations"
                "CalendarPlan.Generator.WebApi"
                "CalendarPlan.Generator.Writers"
            ]

            let! response = client.GetAsync "/api/v2/version"

            let! result = AssertionHelper.GetObjectAsync<VersionInfo> response
            result.Assemblies |> should haveLength 4
            result.Assemblies |> Array.map (_.AssemblyName) |> Array.toSeq |> should equalSeq expectedAssemblies
            for info in result.Assemblies do
                Version.Parse(info.Version) |> should not' (be null)
            result.Runtime |> should equal System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription
        }

    [<Fact>]
    member _.``Get academic years list`` () =
        task {
            use client = testServer.CreateClient()

            let! response = client.GetAsync "/api/v2/academic-years"

            let expected = seq [ "2021-2022"; "2022-2023"; "2023-2024" ]
                           |> Seq.mapi (fun i x -> { Id = i+1; Name = x; IsActive = true })
                           |> Seq.sortByDescending _.Id

            let! result = AssertionHelper.GetObjectAsync<ReferenceListItem<int> list> response
            response.StatusCode |> should equal HttpStatusCode.OK
            result |> should haveLength 3
            result |> List.map _.Id |> should be descending
            result |> should equalSeq expected
        }

    interface IClassFixture<DatabaseFixture>
