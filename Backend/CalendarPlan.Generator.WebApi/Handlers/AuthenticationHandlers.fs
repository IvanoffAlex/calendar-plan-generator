module CalendarPlan.Generator.WebApi.Handlers.AuthenticationHandlers

open CalendarPlan.Generator.WebApi
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Configuration
open Microsoft.AspNetCore.Authentication
open Microsoft.OpenApi.Models
open Oxpecker
open Oxpecker.OpenApi.Configuration

module AuthSchemes =
    let Cookie = "Cookies"
    let Google = "Google"

type AccessRight = { Name: string; CanManage: bool; IsAuthenticated: bool }

type AuthenticationMeta() =
    let metaSummary = "Authentication"
    let metaTags = [| OpenApiTag(Name = "Authentication endpoints") |]

    member this.GetAccessRights =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Return user access rights" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<AccessRight>) ])

let private getConfiguration (ctx: HttpContext) key =
    let configuration = ctx.GetService<IConfiguration>()
    configuration.GetValue(key)

let Meta = AuthenticationMeta()

let SignOut (ctx: HttpContext) =
    let redirectUri = getConfiguration ctx "WebClientUrl"
    ctx.SignOutAsync(AuthSchemes.Cookie, AuthenticationProperties(RedirectUri = redirectUri))

let SigninGoogle (ctx: HttpContext) =
    let redirectUri = getConfiguration ctx "WebClientUrl"
    ctx.ChallengeAsync(AuthSchemes.Google, AuthenticationProperties(RedirectUri = redirectUri))

let GetAccessRights (ctx: HttpContext) =
    let accessRight = { Name = ctx.User.Identity.Name
                        CanManage = HttpContextUtility.IsSupervisors ctx ctx.User
                        IsAuthenticated = ctx.User.Identity.IsAuthenticated }
    ctx.WriteJson accessRight
