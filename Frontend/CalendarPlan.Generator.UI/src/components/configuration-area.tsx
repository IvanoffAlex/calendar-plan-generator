/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-floating-promises */
import * as React from 'react';
import FileDownload from 'js-file-download';
import { Button, Layout, Space, Upload, message } from 'antd';
import { observer } from 'mobx-react-lite';
import { LoadingComponentProps, SuccessAction } from '../interfaces/generic-interfaces';
import { calendarPlanStore } from '../services/calendar-plan-store';
import { calendarPlanDataService } from '../services/calendar-plan-data-service';

export const ConfigurationArea = observer(({ isLoading }: LoadingComponentProps) => {
  function isGenerationAvailable () {
    return !calendarPlanStore.isDataInvalid &&
      !isLoading;
  }

  const configUpload = {
    maxCount: 1,
    showUploadList: false,
    beforeUpload: (file : File) => {
      const isJSON = file.type === 'application/json';
      if (!isJSON) {
        message.error(`${file.name} повинен бути в форматі JSON`);
      }
      return isJSON || Upload.LIST_IGNORE;
    },
    customRequest: (options) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const { onError } = options;
      const onSuccess = options.onSuccess as SuccessAction;
      const file:File = options.file as File;
      const reader = new FileReader();
      reader.onload = (e) => {
        try {
          calendarPlanStore.setSchedule(e.target?.result as string);
          message.success(`${file.name} успішно завантажений`);
          onSuccess('Ok');
        } catch (error) {
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          message.error(`${file.name} має некорректний формат. Помилка: ${error.message}`, 10);
          // eslint-disable-next-line @typescript-eslint/no-unsafe-call
          onError({ event: error });
        }
      };
      reader.readAsText(file as Blob);
    }
  };
  function onDownloadTemplateClick () {
    FileDownload(
      calendarPlanStore.scheduleTemplate,
      calendarPlanDataService.buildCalendarPlanFilename(calendarPlanStore.academicYear, 'schedule', 'json'));
  }
  return <Layout className={'content-layout'}>
        <Space size={'large'}>
            <Upload {...configUpload}>
                <Button>Завантажити конфігураційний файл</Button>
            </Upload>
        </Space>
        <Space size={'large'} className="content-layout-top">
            <Button onClick={() => onDownloadTemplateClick()} disabled={!isGenerationAvailable()}>Отримати конфігураційний файл</Button>
        </Space>
        <Space size={'large'} className="content-layout-top">
            <Button onClick={() => calendarPlanStore.resetSchedule()} disabled={!isGenerationAvailable()}>Очистити</Button>
        </Space>
    </Layout>;
});
