namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20230823194034L, "Add Class column to AcademicYearRange Table")>]
type _20230823194034_AddClassColumnToAcademicYearRangeTable() =
    inherit Migration()

    override this.Up() =
        base.Execute("alter table AcademicYearRange add column Class INTEGER not null default (-1);")
    override this.Down() =
        base.Execute("alter table AcademicYearRange drop column Class;")
