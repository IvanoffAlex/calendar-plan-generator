import * as React from 'react';
import { render } from 'react-dom';

import { Layout } from 'antd';
import { Footer, Header } from 'antd/lib/layout/layout';

import './assets/index.scss';

import { ScheduleArea } from './components/schedule-area';
import { VersionInfoArea } from './components/version-info-area';
import { SubjectEditForm } from './components/subject-edit-form';

const Application: React.FC = () => {
  return (
    <><Layout>
      <Header>
        <h1 className={'light-text align-center'}>Генерація шаблонів календарних планів за розкладом</h1>
      </Header>
      <Layout>
        <ScheduleArea />
      </Layout>
      <Footer className={'align-center'}><VersionInfoArea />© 2021-2024</Footer>
      <SubjectEditForm />
    </Layout></>
  );
};

render(<Application />, document.getElementById('root'));
