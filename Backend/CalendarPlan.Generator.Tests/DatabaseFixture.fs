namespace CalendarPlan.Generator.Tests

open System

type DatabaseFixture() =
    do TestAssets.EnsureTestDatabase

    interface IDisposable with
        member _.Dispose() = ()

type SubjectTableDatabaseFixture() =
    do TestAssets.EnsureTestDatabase
    do TestAssets.FillDataSubjectTable [1..999] None None

    interface IDisposable with
        member _.Dispose() = ()
