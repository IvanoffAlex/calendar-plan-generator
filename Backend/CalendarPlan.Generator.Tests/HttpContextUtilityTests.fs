namespace CalendarPlan.Generator.Tests
open System
open System.IO
open System.IO.Compression
open System.Text
open CalendarPlan.Generator.WebApi
open CalendarPlan.Generator.Core.Types
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Http.Features
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Primitives
open Xunit
open FsUnit.Xunit

type HttpContextUtilityTests() =
    let expectedRange = { To = DateTime.Today; From = DateTime.Today.AddMonths -1 }

    [<Fact>]
    member _.``CreateZipArchive should create it`` () =
        let addZipEntries compileEntry =
            compileEntry ("test", new MemoryStream(Encoding.UTF8.GetBytes "test"))
            compileEntry ("test #2", new MemoryStream(Encoding.UTF8.GetBytes "entry #2"))

        let result = HttpContextUtility.CreateZipArchive addZipEntries

        result |> should not' (be null)

        use zip = new ZipArchive(new MemoryStream(result))
        zip.Entries |> should haveCount 2
        zip.Entries
        |> Seq.map (fun x ->
            let stream = new StreamReader(x.Open(), Encoding.Default)
            (x.Name, x.Crc32, stream.ReadToEnd()))
        |> should equalSeq (seq [("test", 3632233996u, "test"); ("test #2", 1829057889u, "entry #2")])

    [<Fact>]
    member _.``ToBytes should serialize object`` () =
        let sc = ServiceCollection()
        let ctx = DefaultHttpContext()
        ctx.RequestServices <- sc.BuildServiceProvider()

        let result = HttpContextUtility.ToBytes expectedRange

        let json = Encoding.UTF8.GetString result;
        let actual = AssertionHelper.FromJson<Range> json
        actual |> should equal expectedRange

    [<Fact>]
    member _.``SetOutputStreamHttpHeader should add content headers`` () =
        let response = HttpResponseFeature();
        let features = FeatureCollection();
        features.Set<IHttpResponseFeature>(response);
        let ctx = DefaultHttpContext(features);

        HttpContextUtility.SetOutputStreamHttpHeader "alias" ".zip" ctx

        ctx.Response.Headers |> should not' (be Empty)
        ctx.Response.Headers.ContentDisposition |> Seq.exactlyOne |> should startWith "attachment; filename=alias."
        ctx.Response.Headers.ContentDisposition |> Seq.exactlyOne |> should endWith ".zip"
        ctx.Response.Headers.AccessControlExposeHeaders |> Seq.exactlyOne |> should equal "Content-Disposition"
        ctx.Response.Headers.XContentTypeOptions |> Seq.exactlyOne |> should equal "nosniff"
        ctx.Response.Headers.ContentType |> Seq.exactlyOne |> should equal "application/octet-stream"

    [<Fact>]
    member _.``ReadFormBody should get object`` () =
        let sc = ServiceCollection()
        let formKey = "formKey"

        let expectedJson = AssertionHelper.ToJson expectedRange
        let requestKeys = [(formKey, StringValues(expectedJson))]
        let request = System.Linq.Enumerable.ToDictionary(requestKeys, fst, snd)
        let formCollection = FormCollection(request);
        let ctx = DefaultHttpContext()
        ctx.Request.Form <- formCollection
        ctx.RequestServices <- sc.BuildServiceProvider()

        let result = HttpContextUtility.ReadFormBody ctx formKey

        AssertionHelper.Assert<Range> result expectedRange

    [<Fact>]
    member _.``ReadFormFiles should read file`` () =
        let json = AssertionHelper.ToJson expectedRange
        let bytes = Encoding.UTF8.GetBytes json
        use stream = new MemoryStream(bytes)
        let x = FormFile(stream, 0, stream.Length, "json", "range.json")
        let formFileCollection = FormFileCollection()
        formFileCollection.Add x

        let read (file : IFormFile) =
            let name = file.Name
            let filename = file.FileName
            use fileStream = new MemoryStream()
            file.CopyTo(fileStream)
            let fileBytes = fileStream.ToArray()
            let fileJson = Encoding.UTF8.GetString(fileBytes)
            (name, filename, AssertionHelper.FromJson<Range> fileJson)

        let result = HttpContextUtility.MapParallel formFileCollection read
        result |> Seq.iter (fun (name, filename, range) ->
              (name |> should equal "json"
               filename |> should equal "range.json"
               AssertionHelper.Assert<Range> range expectedRange ))
