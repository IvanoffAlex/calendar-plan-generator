﻿module CalendarPlan.Generator.Core.Db

open System
open System.Collections.Generic
open System.Text.Json
open System.Transactions
open Dapper
open Dapper.FSharp.SQLite
open Microsoft.Data.Sqlite
open Microsoft.Extensions.Configuration
open CalendarPlan.Generator.Core.Types
open SimpleMigrations
open SimpleMigrations.DatabaseProvider

type AcademicYearModel = { Range: Range; NonWorkingDays: DayOfWeek seq;  IsActive: bool; }
type RangeModel = {Type: RangeType; DateFrom: DateTime; DateTo: DateTime; Class: int option}

type AcademicYearTable = { Id: int option; AcademicYear: string; NonWorkingDays: string; IsActive: int }
type AcademicYearRangeTable = { Id: int option; AcademicYearId: int; Type: int; DateFrom: string; DateTo: string; Class: int }
with static member Default = { Id = None; AcademicYearId = 0; Type = 0; DateFrom = ""; DateTo = ""; Class = -1 }

type SubjectTable = { Id: int option; Name: string; Class: int; ProgramType: int; CreatedOn: string; ModifiedOn: string; }

type LessonTitleTable = {Id: int option; SubjectId: int; Title: string; Semester: int; IsAnchor: int; Weight: float}

type FSharpDapperFactory(configuration : IConfiguration) =
    do OptionTypes.register()
    let connectionString = configuration.GetValue("Database:Sqlite")
    let academicYearTable = table'<AcademicYearTable> "AcademicYear"
    let academicYearRangeTable = table'<AcademicYearRangeTable> "AcademicYearRange"
    let subjectTable = table'<SubjectTable> "Subject"
    let lessonTitleTable = table'<LessonTitleTable> "LessonTitle"

    member this.AcademicYearTable = academicYearTable
    member this.AcademicYearRangeTable = academicYearRangeTable
    member this.SubjectTable = subjectTable
    member this.LessonTitleTable = lessonTitleTable
    member this.Connection = new SqliteConnection (connectionString)

let private getLastInsertedIdAsync (connection: SqliteConnection) =
    connection.QuerySingleAsync<int> "SELECT last_insert_rowid()"

let private tryRange (rangeDict : IDictionary<RangeType, AcademicYearRangeTable seq>) key =
   match rangeDict.TryGetValue key with
   | true, value ->
      value
      |> Seq.sortByDescending _.Class
      |> Seq.map (fun x ->
         { From = x.DateFrom |> DateTime.Parse
           To = x.DateTo |> DateTime.Parse })
   | _ -> Seq.empty

let private getAcademicYearRangesAsync (db : FSharpDapperFactory) academicYearId classes =
    task {
        printfn "Db -> query academicYear ranges"
        return!
            select {
                for r in db.AcademicYearRangeTable do
                innerJoin a in db.AcademicYearTable on (Some r.AcademicYearId  = a.Id)
                where (a.Id = Some academicYearId)
                andWhere  (isIn r.Class classes)
            } |> db.Connection.SelectAsync<AcademicYearRangeTable, AcademicYearTable>
    }

let GetAcademicYearsAsync statuses (db : FSharpDapperFactory) =
    task {
        printfn "Db -> query get academicYears"
        return!
            select {
                for a in db.AcademicYearTable do
                where (isIn a.IsActive statuses)
                orderByDescending a.AcademicYear
                } |> db.Connection.SelectAsync<AcademicYearTable>
    }

let InsertAcademicYearAsync (db : FSharpDapperFactory) model  =
    task {
        use connection = db.Connection
        connection.Open()
        use scope = new TransactionScope()

        let academicYear =
           { Id = None
             AcademicYear = $"%i{model.Range.From.Year}-%i{model.Range.To.Year}"
             IsActive = match model.IsActive with | true -> 1 | _ -> 0
             NonWorkingDays = match model.NonWorkingDays with
                              | null -> "[]"
                              | _ -> JsonSerializer.Serialize model.NonWorkingDays }

        insert {
             into db.AcademicYearTable
             value academicYear }
        |> connection.InsertAsync |> ignore

        let! academicYearId = getLastInsertedIdAsync connection

        let academicYearRange =
           { Id = None
             AcademicYearId = academicYearId
             Type = (RangeType.Year |> int)
             DateFrom = model.Range.From.ToString("yyyy-MM-dd")
             DateTo = model.Range.To.ToString("yyyy-MM-dd")
             Class = -1 }

        insert {
              into db.AcademicYearRangeTable
              value academicYearRange
        } |> db.Connection.InsertAsync |> ignore

        scope.Complete()

        return academicYearId
    }

let UpdateAcademicYearAsync (db : FSharpDapperFactory) academicYearId (model: AcademicYearModel) =
    task {
        use connection = db.Connection
        connection.Open()
        use scope = new TransactionScope()

        let rangeType = RangeType.Year |> int

        let academicYear =
          { Id = None
            AcademicYear = ""
            NonWorkingDays = match model.NonWorkingDays with
                             | null -> "[]"
                             | _ -> JsonSerializer.Serialize model.NonWorkingDays
            IsActive = match model.IsActive with | true -> 1 | _ -> 0 }

        update {
            for a in db.AcademicYearTable do
            set academicYear
            includeColumn a.NonWorkingDays
            includeColumn a.IsActive
            where (a.Id = Some(academicYearId))
        } |> db.Connection.UpdateAsync |> ignore

        let academicYearRange =
           { AcademicYearRangeTable.Default with
                 DateFrom = model.Range.From.ToString("yyyy-MM-dd")
                 DateTo = model.Range.To.ToString("yyyy-MM-dd") }

        update {
            for r in db.AcademicYearRangeTable do
            set academicYearRange
            includeColumn r.DateFrom
            includeColumn r.DateTo
            where (r.AcademicYearId = academicYearId && r.Type = rangeType && r.Class = -1)
        } |> db.Connection.UpdateAsync |> ignore

        scope.Complete()
    }

let GetEditableRangesAsync db academicYearId =
    task {
        let all = [-1..12]
        let! ranges = getAcademicYearRangesAsync db academicYearId all

        return ranges
               |> Seq.map fst
               |> Seq.map (fun x-> { Type = enum<RangeType> x.Type
                                     Class = match x.Class with
                                             | -1 -> None
                                             | _ -> Some(x.Class)
                                     DateFrom = x.DateFrom |> DateTime.Parse
                                     DateTo = x.DateTo |> DateTime.Parse })
               |> Seq.toArray
    }

let SaveAcademicYearRangesAsync (db : FSharpDapperFactory) academicYearId (model: RangeModel array) =
    task {
        use connection = db.Connection
        connection.Open()
        use scope = new TransactionScope()

        let ranges =
            model
            |> Seq.map (fun x ->
                { AcademicYearRangeTable.Id = None
                  AcademicYearId = academicYearId
                  Type = x.Type |> int
                  DateFrom = x.DateFrom.ToString("yyyy-MM-dd")
                  DateTo = x.DateTo.ToString("yyyy-MM-dd")
                  Class = match x.Class with | Some(v) -> v | _ -> -1 })
            |> Seq.toList

        delete {
            for r in db.AcademicYearRangeTable do
            where (r.AcademicYearId = academicYearId)
        } |> connection.DeleteAsync |> ignore

        insert {
              into db.AcademicYearRangeTable
              values ranges
        } |> db.Connection.InsertAsync |> ignore

        scope.Complete()
    }

let GetAcademicYearAsync db academicYearId studentClass =
    task {
        let currentClass = match studentClass with | Some x -> x | _ -> -1
        let! ranges = getAcademicYearRangesAsync db academicYearId [-1; currentClass]
        let rangeDict = ranges
                        |> Seq.map fst
                        |> Seq.groupBy (fun x -> enum<RangeType> x.Type)
                        |> dict

        let academicYear = ranges
                        |> Seq.map snd
                        |> Seq.head

        return { Id = academicYear.Id.Value
                 AcademicYear = academicYear.AcademicYear
                 Range = tryRange rangeDict RangeType.Year |> Seq.head
                 Holidays = tryRange rangeDict RangeType.Holiday
                 Transitions = tryRange rangeDict RangeType.Transition
                 Vacations = tryRange rangeDict RangeType.Vacation
                 NonWorkingDays = JsonSerializer.Deserialize<DayOfWeek seq> academicYear.NonWorkingDays
                 IsActive = match academicYear.IsActive with | 1 -> true | _ -> false }
    }

let SearchSubjectsAsync (db: FSharpDapperFactory) keyword studentClass (programType: SubjectProgramType) =
    task {
        let pattern = $"%%%s{keyword}%%"
        let programType = programType |> int
        let! subjects =
                select {
                    for s in db.SubjectTable do
                    where (like s.Name pattern)
                    andWhere (s.Class = studentClass)
                    andWhere (s.ProgramType = programType)
                    orderBy s.Name
                    skipTake 0 50
                } |> db.Connection.SelectAsync<SubjectTable>
        return subjects |> Seq.map (fun subject -> { Id = subject.Id.Value; Name = subject.Name; IsActive = true })
    }

let GetSubjectLessons (db: FSharpDapperFactory) id =
    task {
        let! lessons =
            select {
                for l in db.LessonTitleTable do
                where (l.SubjectId = id)
            } |> db.Connection.SelectAsync<LessonTitleTable>

        let! subjects =
            select {
                for s in db.SubjectTable do
                where (s.Id.Value = id)
            } |> db.Connection.SelectAsync<SubjectTable>

        let subject = subjects |> Seq.exactlyOne

        return { Id = subject.Id.Value
                 Name = subject.Name
                 Class = subject.Class
                 ProgramType = enum<SubjectProgramType> subject.ProgramType
                 LessonTitles = lessons
                                |> Seq.map (fun x -> { Title = x.Title
                                                       Semester = x.Semester
                                                       IsAnchor = Convert.ToBoolean(x.IsAnchor)
                                                       Weight = x.Weight })
                                |> Seq.toArray }
    }

let MigrateToLatest<'T> (db : FSharpDapperFactory) =
    let migrationsAssembly = typeof<'T>.Assembly
    use connection = db.Connection
    let databaseProvider = SqliteDatabaseProvider(connection);
    let migrator = SimpleMigrator(migrationsAssembly, databaseProvider);
    migrator.Load()
    migrator.MigrateToLatest()
