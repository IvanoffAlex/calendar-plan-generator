module CalendarPlan.Generator.WebApi.JsonSettings

open System.Text.Encodings.Web
open System.Text.Json
open System.Text.Json.Serialization
open System.Text.Unicode

let DeserializationOptions =
    let options = JsonSerializerOptions(PropertyNamingPolicy = JsonNamingPolicy.CamelCase)
    options.PropertyNameCaseInsensitive <- true
    options.PropertyNamingPolicy <- null
    options.WriteIndented <- true
    options.Encoder <- JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic)
    options.DefaultIgnoreCondition <- JsonIgnoreCondition.WhenWritingNull
    options.Converters.Add(JsonFSharpConverter(JsonUnionEncoding.FSharpLuLike))
    options
