import axios from 'axios';
import { appSettings } from './../app-settings';

export default axios.create({
  baseURL: `${appSettings.BaseWebApiUrl}${appSettings.ApiSuffix}`,
  headers: {
    'Access-Control-Allow-Origin': window.location.host,
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
    'Content-type': 'application/json',
    Accept: 'application/json'
  },
  withCredentials: true
});
