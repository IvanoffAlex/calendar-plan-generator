module CalendarPlan.Generator.WebApi.Handlers.SubjectProgramHandlers

open System.Threading.Tasks
open CalendarPlan.Generator.Core.Db
open CalendarPlan.Generator.Core.Types
open CalendarPlan.Generator.WebApi
open Microsoft.AspNetCore.Http
open Microsoft.OpenApi.Models
open Oxpecker
open Oxpecker.OpenApi.Configuration

type SubjectProgramMeta() =
    let metaSummary = "Subject Programs"
    let metaTags = [| OpenApiTag(Name = "Subject Program Operations") |]

    member this.SearchSubject =
        OpenApiConfig(
            configureOperation = (fun x ->
                x.Parameters.Add (OpenApiParameter(Name = "keyword", In = ParameterLocation.Query))
                SwaggerGenUtilities.ConfigureMetaOperationWithDescription "Search subjects" metaSummary metaTags x),
            responseBodies = [ ResponseBody(typeof<ReferenceListItem<int> seq>) ])

let Meta = SubjectProgramMeta()

let SearchSubject subjectProgramType studentClass (ctx: HttpContext) =
    task {
        let db = ctx.GetService<FSharpDapperFactory>()

        let keyword = ctx.TryGetQueryValue "keyword" |> Option.defaultValue ""
        let subjectProgram = enum<SubjectProgramType> subjectProgramType
        let! subjects = SearchSubjectsAsync db keyword studentClass subjectProgram
        return! json (subjects |> Seq.toArray) ctx
    }
    :> Task
