module CalendarPlan.Generator.Tests.TestAssets
open System
open System.IO
open System.IO.Compression
open Dapper.FSharp.SQLite
open CalendarPlan.Generator.WebApi
open CalendarPlan.Generator.Core.Db
open CalendarPlan.Generator.Core.Types
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.TestHost
open Microsoft.Extensions.Configuration

type UnitTestMarker = interface end

let private dummyClientUrl = "http://dummy/"
let private allPossibleClasses = seq {1..12}

let private buildTestConfig useMigration =
    ConfigurationBuilder()
        .AddInMemoryCollection(
            [ ("WebClientUrl", dummyClientUrl)
              ("Authentication:Google:ClientId", "clientId")
              ("Authentication:Google:ClientSecret", "clientSecret")
              ("Authentication:Google:CallbackUri", "/callback")
              ("Database:UseMigration", useMigration |> string)
              ("Database:Sqlite", "Data Source=TestCalendarPlan.sqlite;") ]
            |> Map.ofList )
        .Build()

let private configureWebBuilder config =
    WebHostBuilder()
        .UseConfiguration(config)
        .ConfigureServices(Startup.ConfigureServices)
        .Configure(Startup.ConfigureApp)

let DummyClientUrl = dummyClientUrl
let AllPossibleClasses = allPossibleClasses

let ExportToImportExcelItems excelItems withSections =
    let generateLessonTitle title index =
        let sectionIndex =
            match withSections with
            | true -> (index - 1) / 4 + 1
            | _ -> 1
        { SectionIndex = sectionIndex
          SectionTitle = $"%s{title} - Section #%i{sectionIndex}"
          LessonIndex = index
          LessonTitle = $"%s{title} #%i{index}" }
    excelItems |> Seq.map (fun x -> (x.Subject, x.Indexes|> Seq.map (generateLessonTitle x.Subject)))

let ToBytes (entry: ZipArchiveEntry) =
    use scheduleItemStream = entry.Open()
    use memoryStream = new MemoryStream()
    scheduleItemStream.CopyTo memoryStream
    memoryStream.ToArray()

let RetrieveEmbeddedResource name =
    use resourceStream = typeof<UnitTestMarker>.Assembly.GetManifestResourceStream
                             $"{typeof<UnitTestMarker>.Namespace}.Assets.{name}"
    use stream = new MemoryStream()
    resourceStream.CopyTo stream
    stream.ToArray()

let RetrieveEmbeddedResourceContent name =
    use resourceStream = typeof<UnitTestMarker>.Assembly.GetManifestResourceStream
                             $"{typeof<UnitTestMarker>.Namespace}.Assets.{name}"
    use tr = new StreamReader(resourceStream)
    let content = tr.ReadToEnd()
    content

let EnsureTestDatabase =
    let config = buildTestConfig true
    let webBuilder = configureWebBuilder config
    let host = webBuilder.Build()
    Startup.WithMigration host.Services

let CreateTestServer =
    let config = buildTestConfig false
    let webBuilder = configureWebBuilder config
    new TestServer(webBuilder)

let CreateDbFactory : FSharpDapperFactory =
    downcast CreateTestServer.Services.GetService(typeof<FSharpDapperFactory>)

let FillDataSubjectTable (numbers: int seq) classModule programTypeModule =
    let db = CreateDbFactory
    let subjects =
        numbers
        |> Seq.map (fun x -> (x, x.ToString("000")))
        |> Seq.map (fun (number, name) ->
             { Id = None
               Name = $"Subject Title %s{name}"
               Class = 1 + number % (classModule |> Option.defaultValue 3)
               ProgramType = number % (programTypeModule |> Option.defaultValue 4)
               CreatedOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
               ModifiedOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") })
        |> Seq.toList
    delete {
        for p in db.SubjectTable do
        deleteAll
    } |> db.Connection.DeleteAsync |> Async.AwaitTask |> Async.RunSynchronously |> ignore
    insert {
        into db.SubjectTable
        values subjects
    } |> db.Connection.InsertAsync |> Async.AwaitTask |> Async.RunSynchronously |> ignore
