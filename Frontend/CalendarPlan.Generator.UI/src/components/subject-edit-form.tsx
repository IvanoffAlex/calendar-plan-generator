import * as React from 'react';
import { Button, Col, Input, InputNumber, Modal, Row, Select } from 'antd';
import { observer } from 'mobx-react-lite';
import { calendarPlanStore } from '../services/calendar-plan-store';
import { Guid } from 'guid-typescript/dist/guid';

export const SubjectEditForm = observer(() => {
  const { Option } = Select;
  const gutter = 8;

  function onCancel () {
    calendarPlanStore.setSelectedScheduleItem(null);
  }

  function onApply () {
    calendarPlanStore.applyItemToSchedule(calendarPlanStore.selectedScheduleItem);
  }

  return (
    calendarPlanStore.selectedScheduleItem &&
    <Modal title={'Редагування урока'} width={900}
      footer={[
        <Button key="back" onClick={onCancel}>
          Відміна
        </Button>,
        <Button key="submit" type="primary" onClick={onApply}>
          Застосувати
        </Button>
      ]}
      open={calendarPlanStore.selectedScheduleItem != null}
      onOk={() => onApply()} onCancel={() => onCancel()}>
      <Row gutter={gutter} className={'content-layout'} >
        <Col span={4} className={'align-right'}>Предмет:</Col>
        <Col span={8}><Input placeholder={'Назва'} style={{ width: 200 }}
          value={calendarPlanStore.selectedScheduleItem?.Subject}
          onChange={(e => calendarPlanStore.setSelectedItemSubject(e.target.value))}/></Col>
        <Col span={4} className={'align-right'}>№ урока:</Col>
        <Col span={8}><InputNumber min={1} max={10}
          value={calendarPlanStore.selectedScheduleItem?.LessonNumber}
          onChange={(value => calendarPlanStore.setSelectedItemLessonNumber(value ?? 1))}/></Col>
      </Row>
      <Row gutter={gutter} className={'content-layout'} >
        <Col span={4} className={'align-right'}>День:</Col>
        <Col span={8}><Select style={{ width: 200 }}
              value={calendarPlanStore.selectedScheduleItem?.Day}
              onChange={(value => calendarPlanStore.setSelectedItemDay(value))}>
            <Option value={1}>Понеділок</Option>
            <Option value={2}>Вівторок</Option>
            <Option value={3}>Середа</Option>
            <Option value={4}>Четвер</Option>
            <Option value={5}>П&apos;ятниця</Option>
          </Select>
        </Col>
        <Col span={4} className={'align-right'}>Навантаження:</Col>
        <Col span={8}><Select style={{ width: 200 }}
              value={calendarPlanStore.selectedScheduleItem?.Load}
              onChange={(value => calendarPlanStore.setSelectedItemLoad(value))}>
            <Option value={1}>(1.0) Одна година</Option>
            <Option value={0.5}>(0.5) Половина години</Option>
            <Option value={0.25}>(0.25) Чверть години</Option>
          </Select></Col>
      </Row>
      <Row gutter={gutter} className={'content-layout'} >
        <Col span={4} className={'align-right'}>Періодичність:</Col>
        <Col span={8}>
          <Select style={{ width: 200 }}
              value={calendarPlanStore.selectedScheduleItem?.Period}
              onChange={(value => calendarPlanStore.setSelectedItemPeriod(value))}>
            <Option value={1}>Кожного тиждня</Option>
            <Option value={2}>1 раз на 2 тиждня</Option>
            <Option value={3}>1 раз на 3 тиждня</Option>
            <Option value={4}>1 раз на 4 тиждня</Option>
          </Select>
        </Col>
        <Col span={4} className={'align-right'} hidden={calendarPlanStore.isOffsetHidden}>Неділя:</Col>
        <Col span={8} hidden={calendarPlanStore.isOffsetHidden}><InputNumber
          value={calendarPlanStore.selectedScheduleItem?.Offset}
          onChange={(value => calendarPlanStore.setSelectedItemOffset(value ?? 1))}
          min={1} max={calendarPlanStore.selectedScheduleItem?.Period}/></Col>
      </Row>
      <Row gutter={gutter} className={'content-layout error-layout'} hidden={calendarPlanStore.isValidForm}>
        <Col span={24}>
          {calendarPlanStore.errors.map(txt => <p key={Guid.create().toString()}>{txt}</p>)}
        </Col>
      </Row>
    </Modal>
  );
});
