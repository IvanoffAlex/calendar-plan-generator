module CalendarPlan.Generator.WebApi.Handlers.CalendarPlanHandlers

open System.IO
open System.Threading.Tasks
open CalendarPlan.Generator.Core.Types
open FSharp.Control
open Microsoft.AspNetCore.Http
open Microsoft.OpenApi.Models
open Oxpecker
open CalendarPlan.Generator.Core
open CalendarPlan.Generator.Core.Db
open CalendarPlan.Generator.Writers
open CalendarPlan.Generator.WebApi
open Oxpecker.OpenApi.Configuration

type CalendarPlanMeta() =
    let metaSummary = "Calendar Plans"
    let metaTags = [| OpenApiTag(Name = "Calendar Plan Operations") |]

    member this.ExportCalendarPlan =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Generate empty calendar plan as Word document" metaSummary metaTags,
            requestBody = RequestBody(typeof<Schedule>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<Stream>, [| HttpContextUtility.MimeTypeOctetStream |]) ])

    member this.ExportExcelCalendarPlan =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Generate empty calendar plan schema as pack of Excel documents in zip file" metaSummary metaTags,
            requestBody = RequestBody(typeof<Schedule>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<Stream>, [| HttpContextUtility.MimeTypeOctetStream |]) ])

    member this.TransformExcelFilesToWord =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Generate Word document by schedule and prefilled Excel documents" metaSummary metaTags,
            responseBodies = [ ResponseBody(typeof<Stream>, [| HttpContextUtility.MimeTypeOctetStream |]) ])

    member this.ExportAllMonthsHours =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Export all month hour reports as Word document" metaSummary metaTags,
            requestBody = RequestBody(typeof<MonthsReportSchedule>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<Stream>, [| HttpContextUtility.MimeTypeOctetStream |]) ])

    member this.ExportMonthHours =
        OpenApiConfig(
            configureOperation = SwaggerGenUtilities.ConfigureMetaOperationWithDescription
                "Export single month hour reports as Word document" metaSummary metaTags,
            requestBody = RequestBody(typeof<MonthsReportSchedule>, [| HttpContextUtility.MimeTypeJson |], false),
            responseBodies = [ ResponseBody(typeof<Stream>, [| HttpContextUtility.MimeTypeOctetStream |]) ])

let Meta = CalendarPlanMeta()

let private readImportExcelItems (file : IFormFile) =
    let subject = file.Name.Substring(0, file.Name.LastIndexOf('_'))
    (subject, DocumentWriter.ReadExcelData file.CopyTo)

let private compileZipEntries compileEntry academicYear (plan: Schedule) excelItems =
    compileEntry ($"%s{academicYear}.json",
                  new MemoryStream(HttpContextUtility.ToBytes plan))
    excelItems |> Seq.iter (fun item ->
        compileEntry ($"%s{item.Subject}_%i{item.Semester}.xlsx",
                      DocumentWriter.WriteExcelItemToStream item))

let ExportCalendarPlan (ctx: HttpContext) =
        task {
            let! plan = ctx.BindJson<Schedule>()
            let db = ctx.GetService<FSharpDapperFactory>()
            let! academicYear = GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
            let subjects = Calculation.CalculateSubjects plan academicYear None
            let calendarPlanDocument = Mapping.ToCalendarPlanDocument subjects

            use memoryStream = new MemoryStream()
            DocumentWriter.WriteCalendarPlanToStream calendarPlanDocument memoryStream

            memoryStream.Seek(0L, SeekOrigin.Begin) |> ignore
            HttpContextUtility.SetOutputStreamHttpHeader academicYear.AcademicYear ".docx" ctx
            return! ctx.WriteStream(true, memoryStream, None, None)
        }
        :> Task

let ExportExcelCalendarPlan (ctx: HttpContext) =
    task {
        let! plan = ctx.BindJson<Schedule>()

        let db = ctx.GetService<FSharpDapperFactory>()
        let! academicYear = GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
        let subjects = Calculation.CalculateSubjects plan academicYear None
        let excelItems = Mapping.ToExcelItems academicYear subjects

        let addZipEntries compileEntry =
            compileZipEntries compileEntry academicYear.AcademicYear plan excelItems
        let zipBlob = HttpContextUtility.CreateZipArchive addZipEntries

        HttpContextUtility.SetOutputStreamHttpHeader academicYear.AcademicYear ".zip" ctx
        return! ctx.WriteBytes zipBlob
    }
    :> Task

let TransformExcelFilesToWord (ctx: HttpContext) =
    task {
        let plan = HttpContextUtility.ReadFormBody<Schedule> ctx "calendarPlan"

        let db = ctx.GetService<FSharpDapperFactory>()
        let! academicYear = GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
        let subjects = Calculation.CalculateSubjects plan academicYear None
        let excelData = HttpContextUtility.MapParallel ctx.Request.Form.Files readImportExcelItems
        let calendarPlanDocument = Mapping.ToCalendarPlanDocumentFromExcelData subjects excelData

        use memoryStream = new MemoryStream()
        DocumentWriter.WriteCalendarPlanToStream calendarPlanDocument memoryStream

        memoryStream.Seek(0L, SeekOrigin.Begin) |> ignore
        HttpContextUtility.SetOutputStreamHttpHeader academicYear.AcademicYear ".docx" ctx
        return! ctx.WriteStream(true, memoryStream, None, None)
    }
    :> Task

let ExportAllMonthsHours (ctx : HttpContext) =
    task {
        let! plan = ctx.BindJson<MonthsReportSchedule>()
        let db = ctx.GetService<FSharpDapperFactory>()
        let! academicYear = GetAcademicYearAsync db plan.Schedule.AcademicYearId plan.Schedule.StudentClass
        let subjects = Calculation.CalculateSubjects plan.Schedule academicYear None
        let allMonthsReport = Mapping.ToMonthsReport academicYear subjects

        use memoryStream = new MemoryStream()
        DocumentWriter.WriteMonthReportToStream allMonthsReport academicYear plan.Schedule.StudentClass.Value plan.Meta memoryStream

        memoryStream.Seek(0L, SeekOrigin.Begin) |> ignore
        HttpContextUtility.SetOutputStreamHttpHeader academicYear.AcademicYear ".docx" ctx
        return! ctx.WriteStream(true, memoryStream, None, None)
    }
    :> Task

let ExportMonthHours (month : int) (ctx : HttpContext) =
    task {
        let! plan = ctx.BindJson<MonthsReportSchedule>()
        let db = ctx.GetService<FSharpDapperFactory>()
        let! academicYear = GetAcademicYearAsync db plan.Schedule.AcademicYearId plan.Schedule.StudentClass
        let monthSubjects = Calculation.CalculateSubjects plan.Schedule academicYear (Some month)
        let reportData = Mapping.ToMonthMReport month monthSubjects

        use memoryStream = new MemoryStream()
        DocumentWriter.WriteMonthReportToStream reportData academicYear plan.Schedule.StudentClass.Value plan.Meta memoryStream

        memoryStream.Seek(0L, SeekOrigin.Begin) |> ignore
        HttpContextUtility.SetOutputStreamHttpHeader academicYear.AcademicYear ".docx" ctx
        return! ctx.WriteStream(true, memoryStream, None, None)
    }
    :> Task
