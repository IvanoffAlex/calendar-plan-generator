﻿namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20220904142545L, "Seed AcademicYearRange Table 2022")>]
type _20220904142545_SeedAcademicYearRangeTable2022() =
    inherit Migration()

    let upScript = @"
update AcademicYearRange set DateTo = '2023-06-02' WHERE Id = 15;

insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (16, '2022-2023', 1, '2022-10-14', '2022-10-14');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (17, '2022-2023', 1, '2023-03-06', '2023-03-06');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (18, '2022-2023', 1, '2023-03-07', '2023-03-07');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (19, '2022-2023', 1, '2023-03-08', '2023-03-08');

insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (20, '2022-2023', 1, '2023-05-01', '2023-05-01');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (21, '2022-2023', 1, '2023-05-02', '2023-05-02');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (22, '2022-2023', 1, '2023-05-03', '2023-05-03');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (23, '2022-2023', 1, '2023-05-04', '2023-05-04');

insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (24, '2022-2023', 1, '2023-05-08', '2023-05-08');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (25, '2022-2023', 1, '2023-05-09', '2023-05-09');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (26, '2022-2023', 1, '2023-05-10', '2023-05-10');

insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (27, '2022-2023', 3, '2022-10-24', '2022-10-30');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (28, '2022-2023', 3, '2022-12-24', '2023-01-15');
insert into AcademicYearRange (Id, AcademicYear, Type, DateFrom, DateTo) values (29, '2022-2023', 3, '2023-03-27', '2023-04-02');"

    override this.Up() =
        base.Execute(upScript)
    override this.Down() =
        base.Execute(@"update AcademicYearRange set DateTo = '2023-05-31' WHERE Id = 15;
delete from AcademicYearRange where Id > 15")
