namespace CalendarPlan.Generator.WebApi.Handlers.Extensions

open System.Runtime.CompilerServices
open CalendarPlan.Generator.WebApi.Handlers
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.OpenApi.Models
open Oxpecker

type IApplicationBuilderExtensions =

    [<Extension>]
    static member inline ConfigureOxpecker (appBuilder: IApplicationBuilder) =
        appBuilder
            .UseRouting()
            .UseAuthentication()
            .UseAuthorization()
            .Use(RootHandlers.ErrorHandler)
            .UseOxpecker(Routes.Endpoints)
            .UseSwagger()
            .UseSwaggerUI(fun x -> x.SwaggerEndpoint("/swagger/v2/swagger.json", "Calendar Plan API v2"))
            .Run(RootHandlers.NotFoundHandler)

    [<Extension>]
    static member inline ConfigureOxpeckerServices (services : IServiceCollection) =
        services
            .AddRouting()
            .AddOxpecker()
            .AddEndpointsApiExplorer()
            .AddSwaggerGen(fun x -> x.SwaggerDoc("v2", OpenApiInfo(Title = "Calendar Plan API", Version = "v2")))
