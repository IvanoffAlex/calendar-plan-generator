namespace CalendarPlan.Generator

open System.Collections.Generic
open System.Reflection

type AssemblyInfo = { AssemblyName: string; Version: string }
type VersionInfo = { Assemblies: AssemblyInfo array; Runtime: string }
type SystemInfo() =
    let assemblyNameTemplate = "CalendarPlan.Generator"
    let getAllAssemblies (assembly: Assembly) (acc: IDictionary<string, Assembly>) searchFilter =
        let children =
            assembly.GetReferencedAssemblies()
            |> Array.filter (fun x -> searchFilter x && (acc.ContainsKey x.FullName |> not))
            |> Array.map Assembly.Load
        children |> Array.iter (fun x -> acc.Add(x.FullName, x))
        children

    let rec getAssemblies acc assembly  =
        getAllAssemblies assembly acc (fun x -> x.Name.StartsWith assemblyNameTemplate)
        |> Array.collect (getAssemblies acc)

    member _.Info with get(assembly : Assembly) =
        let acc = Dictionary<string, Assembly>()
        acc.Add(assembly.FullName, assembly)
        getAssemblies acc assembly |> ignore

        { Assemblies = acc.Values
                        |> Seq.map (_.GetName())
                        |> Seq.map (fun x -> {
                            AssemblyName = x.Name
                            Version = x.Version.ToString(3)
                        })
                        |> Seq.sortBy (_.AssemblyName)
                        |> Seq.toArray
          Runtime = System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription }
