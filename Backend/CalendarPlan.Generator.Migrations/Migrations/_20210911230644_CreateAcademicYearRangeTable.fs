﻿namespace CalendarPlan.Generator.Migrations
open SimpleMigrations

[<Migration(20210911230644L, "Create AcademicYearRange Table")>]
type _20210911230644_CreateAcademicYearRangeTable() =
    inherit Migration()
    let upScript = @"
create table AcademicYearRange
(
    Id           INTEGER not null
        constraint PK_academic_year_range
            primary key autoincrement,
    AcademicYear TEXT,
    Type         INTEGER not null,
    DateFrom     TEXT,
    DateTo       TEXT
);

create index AcademicYearRange_AcademicYear_index
    on AcademicYearRange (AcademicYear);"

    override this.Up() =
        base.Execute(upScript)
    override this.Down() =
        base.Execute(@"drop table AcademicYearRange")
