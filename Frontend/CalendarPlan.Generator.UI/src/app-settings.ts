export class AppSettings {
  get BaseWebApiUrl () : string {
    const originFromEnv = process.env.CALENDAR_PLAN_GENERATOR_API as string;
    const origin = window.location.origin;
    return originFromEnv ?? origin;
  }

  get ApiSuffix () : string {
    return '/api/v2';
  }
}

export const appSettings = new AppSettings();
