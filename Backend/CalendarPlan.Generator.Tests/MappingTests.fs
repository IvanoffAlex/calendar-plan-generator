namespace CalendarPlan.Generator.Tests
open System
open System.Collections.Generic
open CalendarPlan.Generator.Core.Types
open CalendarPlan.Generator.Core
open Xunit
open FsUnit.Xunit

type MappingTests() =
    let db = TestAssets.CreateDbFactory

    let expectedSubjects = seq [("Subj_1",1.);("Subj_4",1.0);("Subj_5",1.);("Subj_2",0.5);("Subj_3",0.25)]
    let plan =
        {
            AcademicYearId = 1
            StudentClass = None
            Schedules = seq [
                { LessonNumber = 1; Subject = "Subj_1"; Day = DayOfWeek.Monday; Offset = 1; Period = 1; Load = Some 1 }
                { LessonNumber = 1; Subject = "Subj_2"; Day = DayOfWeek.Thursday; Offset = 1; Period = 1; Load = Some 0.5 }
                { LessonNumber = 1; Subject = "Subj_3"; Day = DayOfWeek.Wednesday; Offset = 1; Period = 1; Load = Some 0.25 }
                { LessonNumber = 1; Subject = "Subj_4"; Day = DayOfWeek.Thursday; Offset = 1; Period = 1; Load = Some 1 }
                { LessonNumber = 1; Subject = "Subj_5"; Day = DayOfWeek.Friday; Offset = 1; Period = 1; Load = Some 1 }
            ]
        }

    [<Fact>]
    member _.``ToMonthReport mapping`` () =
        task {
            let month = DateTime(2021,10,1)
            let expected: IDictionary<DateTime, seq<SubjectEvents>> =
                AssertionHelper.RetrieveExpectedTestAsset "Mapping.ToMonthReport.json"

            let expectedEvents = expected.Item(month) |> Seq.toList
            let! academicYear = Db.GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
            let monthSubjects = Calculation.CalculateSubjects plan academicYear (Some month.Month)

            let result = Mapping.ToMonthMReport month.Month monthSubjects

            result |> should haveCount 1
            let resultEvents = result.Item(month)
            AssertionHelper.AssertSubjectEvents resultEvents expectedEvents expectedSubjects
        }

    [<Fact>]
    member _.``ToMonthsReport mapping`` () =
        task {
            let expected: IDictionary<DateTime, seq<SubjectEvents>> =
                AssertionHelper.RetrieveExpectedTestAsset "Mapping.ToMonthsReport.json"
            let expectedMonths = seq [0..8] |> Seq.map (fun x -> DateTime(2021,9,1).AddMonths(x))
            let! academicYear = Db.GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
            let monthSubjects = Calculation.CalculateSubjects plan academicYear None

            let result = Mapping.ToMonthsReport academicYear monthSubjects

            result |> should haveCount 9
            result.Keys |> should equalSeq expectedMonths
            for entry in result do
                let expectedEvents = expected.Item(entry.Key) |> Seq.toList
                AssertionHelper.AssertSubjectEvents entry.Value expectedEvents expectedSubjects
        }

    [<Fact>]
    member _.``ToCalendarPlanDocument mapping`` () =
        task {
            let expected: CalendarPlanDocument = AssertionHelper.RetrieveExpectedTestAsset "Mapping.ToCalendarPlanDocument.json"
            let! academicYear = Db.GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
            let subjects = Calculation.CalculateSubjects plan academicYear None

            let result = Mapping.ToCalendarPlanDocument subjects

            AssertionHelper.Assert<CalendarPlanDocument> result expected
        }

    [<Fact>]
    member _.``ToExcelItems mapping`` () =
        task {
            let expected = AssertionHelper.RetrieveExpectedTestAsset "Mapping.ToExcelItems.json"
            let expectedSemesters = seq [1..2] |> Seq.collect (fun x -> seq [1..5] |> Seq.map (fun n -> ($"Subj_{n}", x)))
            let! academicYear = Db.GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
            let subjects = Calculation.CalculateSubjects plan academicYear None

            let result = Mapping.ToExcelItems academicYear subjects

            let assertResult = result |> Seq.map (fun x -> (x.Subject,x.Semester), x.Indexes) |> dict
            assertResult |> should haveCount 10
            assertResult.Keys |> Seq.sortBy fst |> Seq.sortBy snd |> should equalSeq expectedSemesters
            for entry in assertResult do
                let subject, semester = entry.Key
                let expectedIndexes =
                    expected
                    |> Seq.filter (fun x -> x.Subject = subject && x.Semester = semester)
                    |> Seq.exactlyOne
                entry.Value |> should equalSeq expectedIndexes.Indexes
        }

    [<Theory>]
    [<InlineData(true, "WithSections")>]
    [<InlineData(false, "WithoutSections")>]
    member _.``ToCalendarPlanDocumentFromExcelData mapping`` withSection resourceNamePart =
        task {
            let expected: CalendarPlanDocument =
                AssertionHelper.RetrieveExpectedTestAsset $"Mapping.ToCalendarPlanDocumentFromExcelData%s{resourceNamePart}.json"
            let! academicYear = Db.GetAcademicYearAsync db plan.AcademicYearId plan.StudentClass
            let subjects = Calculation.CalculateSubjects plan academicYear None
            let exportExcelItems = Mapping.ToExcelItems academicYear subjects
            let importExcelItems = TestAssets.ExportToImportExcelItems exportExcelItems withSection

            let result = Mapping.ToCalendarPlanDocumentFromExcelData subjects importExcelItems

            AssertionHelper.Assert<CalendarPlanDocument> result expected
        }

    interface IClassFixture<DatabaseFixture>
