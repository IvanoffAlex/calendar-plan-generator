module CalendarPlan.Generator.WebApi.ViewModels

type ErrorResultViewModel = { StatusCode: int; Message: string }
