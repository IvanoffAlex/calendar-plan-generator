namespace CalendarPlan.Generator.Tests

open CalendarPlan.Generator.Core.Types
open Xunit
open FsUnit.Xunit

type SubjectProgramsEndpointTests() =
    let testServer = TestAssets.CreateTestServer

    [<Theory>]
    [<InlineData("", 1, SubjectProgramType.Retention, 50)>]
    [<InlineData("Title 1", 2, SubjectProgramType.General, 9)>]
    [<InlineData("1", 3, SubjectProgramType.Retention, 29)>]
    [<InlineData("05", 1, SubjectProgramType.First, 1)>]
    member _.``Search Subjects should return suitable reference info``(keyword, studentClass, programType, expectedCount) =
        task {
            use client = testServer.CreateClient()
            let url = $"/api/v2/subject-programs/%i{programType |> int}/classes/%i{studentClass}/subjects?keyword=%s{keyword}"

            let! response = client.GetAsync(url)

            let! items = AssertionHelper.GetObjectAsync<ReferenceListItem<int> seq> response
            items |> should haveCount expectedCount
            items
            |> Seq.filter (fun x -> x.Name.Contains keyword)
            |> Seq.toArray
            |> should haveLength expectedCount
        }

    interface IClassFixture<SubjectTableDatabaseFixture>
