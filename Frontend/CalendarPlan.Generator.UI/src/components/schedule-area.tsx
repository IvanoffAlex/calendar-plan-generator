/* eslint-disable @typescript-eslint/no-misused-promises */
import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import FileDownload from 'js-file-download';
import { Guid } from 'guid-typescript/dist/guid';

import { Col, Divider, Layout, Row, Select, Button, Space, InputNumber } from 'antd';

import { calendarPlanStore } from '../services/calendar-plan-store';
import { calendarPlanDataService } from '../services/calendar-plan-data-service';
import { DayOfWeek, ScheduleItem } from '../interfaces/calendar-plan-interfaces';
import { ConfigurationArea } from '../components/configuration-area';
import { ExcelPlanProcessor } from '../components/excel-plan-processor';
import { ScheduleItemRenderer } from '../components/schedule-item-renderer';
import { MonthReportArea } from '../components/month-report-area';
import { AuthenticationArea } from './authentication-info';
import { DefaultOptionType } from 'antd/es/select';

export const ScheduleArea = observer(() => {
  const { Option } = Select;
  const initialOptions: typeof Option[] = [];
  const [academicYearOptions, setAcademicYearOptions] = useState(initialOptions);
  const scheduleItemTemplate: ScheduleItem = { LessonNumber: 1, Subject: '', Load: 1, Day: DayOfWeek.Monday, Period: 1, Offset: 1 };

  const [isLoading, setIsLoading] = useState(false);

  function isGenerationAvailable () {
    return !calendarPlanStore.isDataInvalid &&
      !isLoading;
  }

  function onAddNewScheduleItem () {
    const newLesson: ScheduleItem = Object.assign({ Id: Guid.create() }, scheduleItemTemplate);
    calendarPlanStore.setSelectedScheduleItem(newLesson);
  }

  async function onExportClick () {
    return calendarPlanDataService.exportCalendarPlan(calendarPlanStore.schedule)
      .then(response => {
        let filename = calendarPlanDataService.buildCalendarPlanFilename(calendarPlanStore.academicYear, 'calendar-plan-dates', 'docx');
        filename = response.headers['content-disposition']?.split('filename=')[1] ?? filename;
        FileDownload(response.data, filename);
      });
  }

  async function onExportExcelTemplatesClick () {
    return calendarPlanDataService.exportCalendarPlanAsExcel(calendarPlanStore.schedule)
      .then(response => {
        let filename = calendarPlanDataService.buildCalendarPlanFilename(calendarPlanStore.academicYear, 'report', 'zip');
        filename = response.headers['content-disposition']?.split('filename=')[1] ?? filename;
        FileDownload(response.data, filename);
      });
  }

  useEffect(() => {
    const fetchAcademicYears = async () => {
      setIsLoading(true);
      const result = await calendarPlanDataService.getAcademicYears();
      setAcademicYearOptions(result.data.map((value, index): typeof Option => {
        if (index === 0) {
          calendarPlanStore.setAcademicYear(value);
        }
        return <Option key={index} value={value.Id}>{value.Name}</Option>;
      }));
      setIsLoading(false);
    };

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    fetchAcademicYears();
  }, [Option]);

  function onSelectAcademicYear (id: number, option: DefaultOptionType) {
    calendarPlanStore.setAcademicYear({ Id: id, Name: option.children?.toString() ?? '', IsActive: true });
  }

  return <Row>
    <Col flex="1">
      <Divider orientation="left">Конфігураційний файл</Divider>
      <ConfigurationArea isLoading={isLoading} />
      <Divider orientation="left">Обробка Excel файлів</Divider>
      <ExcelPlanProcessor isLoading={isLoading} />
      <Divider orientation="left">Адміністрування</Divider>
      <AuthenticationArea />
    </Col>
    <Col flex="5" style={{ minHeight: 600 }}>
      <Layout className={'content-layout'}>
        <Space className={'content-layout'} >
          <span className={'form-label'}>Навчальний рік:</span>
          <Select
            value={calendarPlanStore.academicYearId}
            onChange={((value, option) => onSelectAcademicYear(value, option))}
            loading={isLoading}
            disabled={calendarPlanStore.isDisabledSelectedYear}
            style={{ width: 120 }}>
            {academicYearOptions}
          </Select>
          <span className={'form-label'}>Клас учня:</span>
          <InputNumber min={1} max={12} placeholder={'Клас'} style={{ width: 70 }}
            value={calendarPlanStore.studentClass}
            onChange={((value) => calendarPlanStore.setStudentClass(value))}
            />
        </Space>
        <Divider orientation="left">Дії</Divider>
        <Space size={'large'}>
          <Button onClick={() => onExportClick()} type="default" disabled={!isGenerationAvailable()}>Сгенерувати шаблон</Button>
          <Button onClick={() => onExportExcelTemplatesClick()} type="default" disabled={!isGenerationAvailable()}>Сгенерувати шаблони &quot;Єдиної Школи&quot;</Button>
          <MonthReportArea isLoading={isLoading} />
        </Space>
        <Divider orientation="left"></Divider>
        <Button onClick={() => onAddNewScheduleItem()} type='primary' style={{ width: 200 }}>Додати урок до розкладу</Button>
        <Divider orientation="left">Розклад</Divider>
        {calendarPlanStore.schedule?.Schedules?.map(item =>
          <ScheduleItemRenderer key={Guid.create().toString()} item={item}></ScheduleItemRenderer>
        )}
      </Layout>
    </Col>
  </Row>;
});
