namespace CalendarPlan.Generator.Tests
open System
open CalendarPlan.Generator.Core
open CalendarPlan.Generator.Core.Types
open Xunit
open FsUnit.Xunit

type AllClassesExcept1st() as this =
    inherit TheoryData<int option>()
    do
        let except1stClass x = x <> 1
        let add x = this.Add(Some x)

        this.Add(None)
        TestAssets.AllPossibleClasses
            |> Seq.filter except1stClass
            |> Seq.iter add

type AcademicYearRangeTableTests() =
    let db = TestAssets.CreateDbFactory

    [<Fact>]
    member _.``Get ranges for 2021-2022 year`` () =
        task {
            let expectedRange = { From = DateTime(2021,9,1); To = DateTime(2022,5,31) }
            let expectedVacations =
               seq [
                   { From = DateTime(2021,10,25); To = DateTime(2021,10,31) }
                   { From = DateTime(2021,12,25); To = DateTime(2022,01,09) }
                   { From = DateTime(2022,02,28); To = DateTime(2022,03,14) }
                   { From = DateTime(2022,03,14); To = DateTime(2022,03,27) }]
            let expectedTransitions =
                seq [{ From = DateTime(2021,10,15); To = DateTime(2021,10,23) }]
            let expectedHolidays =
                seq [ DateTime(2021,10,14); DateTime(2021,12,25)
                      DateTime(2021,12,27); DateTime(2022,01,01)
                      DateTime(2022,01,07); DateTime(2022,03,08) ]
                |> Seq.map (fun x -> { From = x; To = x })

            let! academicYear = Db.GetAcademicYearAsync db 1 None

            academicYear.Range |> should equal expectedRange
            academicYear.Vacations |> should equalSeq expectedVacations
            academicYear.Transitions |> should equalSeq expectedTransitions
            academicYear.Holidays |> should equalSeq expectedHolidays
        }

    [<Fact>]
    member _.``Get ranges for 1 class`` () =
        task {
            let expectedRange = { From = DateTime(2023,9,1); To = DateTime(2024,5,31) }
            let expectedVacations =
               seq [
                   { From = DateTime(2024,02,19); To = DateTime(2024,02,25) }
                   { From = DateTime(2023,10,23); To = DateTime(2023,10,29) }
                   { From = DateTime(2023,12,23); To = DateTime(2024,01,07) }
                   { From = DateTime(2024,03,25); To = DateTime(2024,03,31) } ]

            let! academicYear = Db.GetAcademicYearAsync db 3 (Some(1))

            academicYear.Range |> should equal expectedRange
            academicYear.Vacations |> should equalSeq expectedVacations
            academicYear.Transitions |> should be Empty
            academicYear.Holidays |> should be Empty
        }

    [<Theory; ClassData(typeof<AllClassesExcept1st>)>]
    member _.``Get ranges for all class except 1st`` (studentClass: int option) =
        task {
            let expectedRange = { From = DateTime(2023,9,1); To = DateTime(2024,5,31) }
            let expectedVacations =
               seq [
                   { From = DateTime(2023,10,23); To = DateTime(2023,10,29) }
                   { From = DateTime(2023,12,23); To = DateTime(2024,01,07) }
                   { From = DateTime(2024,03,25); To = DateTime(2024,03,31) }]

            let! academicYear = Db.GetAcademicYearAsync db 3 studentClass

            academicYear.Range |> should equal expectedRange
            academicYear.Vacations |> should equalSeq expectedVacations
            academicYear.Transitions |> should be Empty
            academicYear.Holidays |> should be Empty
        }

    interface IClassFixture<DatabaseFixture>
